<?php
class TradingWidget extends Widget {
	private static $title = null; //don't show a title for this widget by default
	private static $cmsTitle = "Trading Widget";
	private static $description = "Show trading details";
	
	function TradeAccounts(){
		$accounts = ArrayList::create();
		$trade_settings = TradeSetting::get()->filter('IsActive', 1)->filter('StartDate:LessThanOrEqual', date('Y-m-d'));
		foreach($trade_settings as $trade_setting){
			$account = TradeAccount::get()->filter('TradeSettingID', $trade_setting->ID)->find('MemberID', Distributor::currentUserID());
			$unit = $account ? $account->TradeUnit : 0;
			$data = array(
				'Title' => $trade_setting->Title,
				'Unit' => DBField::create_field('Int', $unit)
			);
			$accounts->push(ArrayData::create($data));
		}
		
		return $accounts;
	}
}