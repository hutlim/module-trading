<?php

class BonusConvertTradePointPageLink extends Extension {
	function getBonusConvertTradePoint(){
        $page = BonusConvertTradePointPage::get()->find('ClassName', 'BonusConvertTradePointPage');
        if($page) return $page;
    }
    
    function getBonusConvertTradePointLink() {
        $page = $this->getBonusConvertTradePoint();
        if($page) return $page->Link();
    }
	
    function getBonusConvertTradePointStatement(){
        $page = BonusConvertTradePointStatementPage::get()->find('ClassName', 'BonusConvertTradePointStatementPage');
        if($page) return $page;
    }
    
    function getBonusConvertTradePointStatementLink() {
        $page = $this->getBonusConvertTradePointStatement();
        if($page) return $page->Link();
    }
}
