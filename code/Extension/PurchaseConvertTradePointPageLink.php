<?php

class PurchaseConvertTradePointPageLink extends Extension {
	function getPurchaseConvertTradePoint(){
        $page = PurchaseConvertTradePointPage::get()->find('ClassName', 'PurchaseConvertTradePointPage');
        if($page) return $page;
    }
    
    function getPurchaseConvertTradePointLink() {
        $page = $this->getPurchaseConvertTradePoint();
        if($page) return $page->Link();
    }
	
    function getPurchaseConvertTradePointStatement(){
        $page = PurchaseConvertTradePointStatementPage::get()->find('ClassName', 'PurchaseConvertTradePointStatementPage');
        if($page) return $page;
    }
    
    function getPurchaseConvertTradePointStatementLink() {
        $page = $this->getPurchaseConvertTradePointStatement();
        if($page) return $page->Link();
    }
}
