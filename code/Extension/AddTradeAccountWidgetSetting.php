<?php
class AddTradeAccountWidgetSetting extends Extension {
	private static $db = array(
		'ShowTradeCoinAccountBalance' => 'Boolean',
		'ShowTradePointAccountBalance' => 'Boolean'
	);
	
	function updateFieldLabels(&$labels) {
		$labels['ShowTradeCoinAccountBalance'] = _t('AddTradeAccountWidgetSetting.SHOW_TRADE_COIN_BALANCE', 'Show Trade-Coin Account Balance?');
		$labels['ShowTradePointAccountBalance'] = _t('AddTradeAccountWidgetSetting.SHOW_TRADE_POINT_BALANCE', 'Show Trade-Point Account Balance?');
	}
}
