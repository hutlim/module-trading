<?php
class AddSellShareType extends Extension {
	function requireDefaultRecords(){
		if(!CashAccountType::get()->filter('Locale', 'en_US')->find('Code', 'Sell Share')){
			CashAccountType::create()
			->setField('Code', 'Sell Share')
			->setField('Title', 'Sell Share')
			->setField('Sort', 75)
			->setField('Locale', 'en_US')
			->write();
		}
		
		if(!CashAccountType::get()->filter('Locale', 'zh_CN')->find('Code', 'Sell Share')){
			CashAccountType::create()
			->setField('Code', 'Sell Share')
			->setField('Title', '出售股份')
			->setField('Sort', 145)
			->setField('Locale', 'zh_CN')
			->write();
		}
	}
}
