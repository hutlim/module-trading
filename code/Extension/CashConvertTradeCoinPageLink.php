<?php

class CashConvertTradeCoinPageLink extends Extension {
	function getCashConvertTradeCoin(){
        $page = CashConvertTradeCoinPage::get()->find('ClassName', 'CashConvertTradeCoinPage');
        if($page) return $page;
    }
    
    function getCashConvertTradeCoinLink() {
        $page = $this->getCashConvertTradeCoin();
        if($page) return $page->Link();
    }
	
    function getCashConvertTradeCoinStatement(){
        $page = CashConvertTradeCoinStatementPage::get()->find('ClassName', 'CashConvertTradeCoinStatementPage');
        if($page) return $page;
    }
    
    function getCashConvertTradeCoinStatementLink() {
        $page = $this->getCashConvertTradeCoinStatement();
        if($page) return $page->Link();
    }
}
