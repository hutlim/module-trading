<?php
/**
 * @package trading
 */
class TradeSetting extends DataObject implements PermissionProvider {
    private static $singular_name = "Trading Setting";
    private static $plural_name = "Trading Settings";
    
    private static $db = array(
    	'IsActive' => 'Boolean',
    	'StartDate' => 'Date',
    	'Code' => 'Varchar',
    	'Title' => 'Varchar',
    	'TotalUnit' => 'Int',
    	'SellUnit' => 'Int',
    	'BuyUnit' => 'Int',
    	'SellVolume' => 'TradeCurrency',
    	'BuyVolume' => 'TradeCurrency',
    	'SellFeePercentage' => 'Percentage',
    	'BuyFeePercentage' => 'Percentage',
    	'Margin' => 'TradeCurrency',
    	'CurrentMargin' => 'TradeCurrency',
    	'NextMargin' => 'TradeCurrency',
    	'MarkupType' => "Enum(array('Amount', 'Percentage'))",
    	'MarkupAmount' => 'TradeCurrency',
    	'MarkupPercentage' => 'Percentage',
        'Price' => 'TradeCurrency',
        'ForceBuyingPrice' => 'Boolean',
        'ForceSellingPrice' => 'Boolean'
    );
	
	private static $has_many = array(
		'LockPeriods' => 'TradeLockPeriod'
	);
	
	private static $many_many = array(
		'RestrictMembers' => 'Member'
	);
	
	private static $many_many_extraFields = array(
	    'RestrictMembers' => array(
	    	'IncludeTeam' => 'Boolean',
	        'BuyOrder' => 'Boolean',
	        'SellOrder' => 'Boolean',
	        'PurchaseShare' => 'Boolean',
	        'TradeRequest' => 'Boolean'
	    )
	);
	
	private static $default_sort = "Price DESC";

    private static $searchable_fields = array(
    	'IsActive',
    	'StartDate' => array(
			'field' => 'DateField',
			'filter' => 'DateMatchFilter'
		),
        'Code',
        'Title',
        'SellFeePercentage' => array(
            'filter' => 'GreaterThanOrEqualFilter'
        ),
        'BuyFeePercentage' => array(
            'filter' => 'GreaterThanOrEqualFilter'
        ),
        'Margin' => array(
            'filter' => 'GreaterThanOrEqualFilter'
        ),
        'Price' => array(
            'filter' => 'GreaterThanOrEqualFilter'
        )
    );

    private static $summary_fields = array(
    	'IsActive.Nice',
    	'StartDate.Nice',
		'Code',
        'Title',
        'SellFeePercentage',
        'BuyFeePercentage',
        'TotalUnit',
        'AvailableUnit',
        'Margin',
        'Markup',
        'Price'
    );
	
	private static $casting = array(
		'Markup' => 'TradeCurrency',
		'IsStarted' => 'Boolean',
		'IsClosed' => 'Boolean'
	);
	
	public function fieldLabels($includerelations = true) {
		$labels = parent::fieldLabels($includerelations);
		
		$labels['IsActive'] = _t('TradeSetting.IS_ACTIVE', 'Is Active?');
		$labels['IsActive.Nice'] = _t('TradeSetting.IS_ACTIVE', 'Is Active?');
		$labels['StartDate'] = _t('TradeSetting.START_DATE', 'Start Date');
		$labels['StartDate.Nice'] = _t('TradeSetting.START_DATE', 'Start Date');
		$labels['Code'] = _t('TradeSetting.CODE', 'Code');
		$labels['Title'] = _t('TradeSetting.TITLE', 'Title');
		$labels['TotalUnit'] = _t('TradeSetting.TOTAL_UNIT', 'Total Unit');
		$labels['AvailableUnit'] = _t('TradeSetting.AVAILABLE_UNIT', 'Available Unit');
		$labels['SellUnit'] = _t('TradeSetting.SELL_UNIT', 'Sell Unit');
		$labels['BuyUnit'] = _t('TradeSetting.BUY_UNIT', 'Buy Unit');
		$labels['SellVolume'] = _t('TradeSetting.SELL_VOLUME', 'Sell Volume');
		$labels['BuyVolume'] = _t('TradeSetting.BUY_VOLUME', 'Buy Volume');
		$labels['SellFeePercentage'] = _t('TradeSetting.SELL_PROCESSING_FEE', 'Sell Processing Fee');
		$labels['BuyFeePercentage'] = _t('TradeSetting.BUY_PROCESSING_FEE', 'Buy Processing Fee');
		$labels['Margin'] = _t('TradeSetting.MARGIN', 'Margin');
		$labels['CurrentMargin'] = _t('TradeSetting.CURRENT_MARGIN', 'Current Margin');
		$labels['Markup'] = _t('TradeSetting.MARKUP', 'Markup');
		$labels['MarkupType'] = _t('TradeSetting.MARKUP_TYPE', 'Markup Type');
		$labels['MarkupAmount'] = _t('TradeSetting.MARKUP_AMOUNT', 'Markup Amount');
		$labels['MarkupPercentage'] = _t('TradeSetting.MARKUP_PERCENTAGE', 'Markup Percentage');
		$labels['Price'] = _t('TradeSetting.TRADING_PRICE', 'Trading Price');
		$labels['ForceBuyingPrice'] = _t('TradeSetting.FORCE_BUYING_PRICE', 'Force buying price?');
		$labels['ForceSellingPrice'] = _t('TradeSetting.FORCE_SELLING_PRICE', 'Force selling price?');
		$labels['LockPeriods'] = _t('TradeSetting.LOCK_PERIOD', 'Lock Period');
		$labels['RestrictMembers'] = _t('TradeSetting.RESTRICT_MEMBERS', 'Restrict Members');
		
		return $labels;	
	}

	function getCMSFields() {
		$fields = parent::getCMSFields();
		$fields->dataFieldByName('SellFeePercentage')->setDescription(_t('TradeSetting.BY_PERCENTAGE', 'By Percentage'));
		$fields->dataFieldByName('BuyFeePercentage')->setDescription(_t('TradeSetting.BY_PERCENTAGE', 'By Percentage'));
		$fields->makeFieldReadonly('SellUnit');
		$fields->makeFieldReadonly('BuyUnit');
		$fields->makeFieldReadonly('SellVolume');
		$fields->makeFieldReadonly('BuyVolume');
		$fields->makeFieldReadonly('CurrentMargin');
		$fields->makeFieldReadonly('NextMargin');
		
		if($this->exists()) {
			$fields->makeFieldReadonly('Code');
            $extra_fields = FieldList::create(
            	ReadonlyField::create('Username', _t('TradeSetting.USERNAME', 'Username')),
            	ReadonlyField::create('Name', _t('TradeSetting.NAME', 'Name')),
				OptionsetField::create('ManyMany[IncludeTeam]', _t('TradeSetting.INCLUDE_TEAM', 'Include Team'), array('1' => _t('TradeSetting.YES', 'Yes'), '0' => _t('TradeSetting.NO', 'No'))),
				OptionsetField::create('ManyMany[BuyOrder]', _t('TradeSetting.BUY_ORDER', 'Buy Order'), array('1' => _t('TradeSetting.YES', 'Yes'), '0' => _t('TradeSetting.NO', 'No'))),
				OptionsetField::create('ManyMany[SellOrder]', _t('TradeSetting.SELL_ORDER', 'Sell Order'), array('1' => _t('TradeSetting.YES', 'Yes'), '0' => _t('TradeSetting.NO', 'No'))),
				OptionsetField::create('ManyMany[PurchaseShare]', _t('TradeSetting.PURCHASE_SHARE', 'Purchase Share'), array('1' => _t('TradeSetting.YES', 'Yes'), '0' => _t('TradeSetting.NO', 'No'))),
				OptionsetField::create('ManyMany[TradeRequest]', _t('TradeSetting.TRADE_REQUEST', 'Trade Request'), array('1' => _t('TradeSetting.YES', 'Yes'), '0' => _t('TradeSetting.NO', 'No')))
			);
            $config = GridFieldConfig_RelationEditor::create();
			$config->removeComponentsByType('GridFieldAddNewButton');
            $config->getComponentByType('GridFieldDetailForm')->setFields($extra_fields);
			$config->getComponentByType('GridFieldAddExistingAutocompleter')->setSearchFields(array('Username', 'Email', 'Surname', 'FirstName'));
			$fields->dataFieldByName('RestrictMembers')->setConfig($config);
        }

		return $fields;
	}
	
	function onBeforeWrite(){
		parent::onBeforeWrite();
		if($this->isChanged('NextMargin')){
			$count = intval($this->NextMargin / $this->Margin);
			if($count > 0){
				for($i = 0; $i < $count; $i++){
					$this->CurrentMargin += $this->Margin;
					$this->NextMargin -= $this->Margin;
					$this->Price += $this->Markup;
				}
			}
		}
	}
	
	function getIsStarted(){
		return $this->dbObject('StartDate')->InPast();
	}
	
	function getIsClosed(){
		return $this->getAvailableUnit() <= 0;
	}
	
	function getAvailableUnit(){
		return $this->TotalUnit + $this->BuyUnit - $this->SellUnit;
	}
	
	function getMarkup(){
		if($this->MarkupType == 'Percentage'){
			return $this->Price * $this->MarkupPercentage >= 0.001 ? $this->Price * $this->MarkupPercentage : 0;
		}
		else{
			return $this->MarkupAmount;
		}
	}
	
	function canView($member = false) {
        $extended = $this->extendedCan(__FUNCTION__, $member);
        if($extended !== null) {
            return $extended;
        }
        return Permission::check('VIEW_TradeSetting');
    }

    function canEdit($member = false) {
        $extended = $this->extendedCan(__FUNCTION__, $member);
        if($extended !== null) {
            return $extended;
        }
        return Permission::check('EDIT_TradeSetting');
    }

    function canDelete($member = false) {
        $extended = $this->extendedCan(__FUNCTION__, $member);
        if($extended !== null) {
            return $extended;
        }
        return Permission::check('DELETE_TradeSetting');
    }

    function canCreate($member = false) {
        $extended = $this->extendedCan(__FUNCTION__, $member);
        if($extended !== null) {
            return $extended;
        }
        return Permission::check('CREATE_TradeSetting');
    }

    public function providePermissions() {
        return array(
            'VIEW_TradeSetting' => array(
                'name' => _t('TradeSetting.PERMISSION_VIEW', 'Allow view access right'),
                'category' => _t('TradeSetting.PERMISSIONS_CATEGORY', 'Trade Setting')
            ),
            'EDIT_TradeSetting' => array(
                'name' => _t('TradeSetting.PERMISSION_EDIT', 'Allow edit access right'),
                'category' => _t('TradeSetting.PERMISSIONS_CATEGORY', 'Trade Setting')
            ),
            'DELETE_TradeSetting' => array(
                'name' => _t('TradeSetting.PERMISSION_DELETE', 'Allow delete access right'),
                'category' => _t('TradeSetting.PERMISSIONS_CATEGORY', 'Trade Setting')
            ),
            'CREATE_TradeSetting' => array(
                'name' => _t('TradeSetting.PERMISSION_CREATE', 'Allow create access right'),
                'category' => _t('TradeSetting.PERMISSIONS_CATEGORY', 'Trade Setting')
            )
        );
    }
}
?>