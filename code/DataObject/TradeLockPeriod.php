<?php

class TradeLockPeriod extends DataObject implements PermissionProvider {
    private static $singular_name = "Trade Lock Period";
    private static $plural_name = "Trade Lock Period";
    
    private static $db = array(
		'LockDay' => 'Int'
	);
	
	private static $has_one = array(
		'Group' => 'Group',
		'TradeSetting' => 'TradeSetting'
	);
	
	private static $default_sort = "LockDay DESC";
	
	private static $searchable_fields = array(
    	'LockDay',
    	'GroupID'
    );

    private static $summary_fields = array(
    	'LockDay',
    	'Group.TitleByLang'
    );
	
	public function fieldLabels($includerelations = true) {
		$labels = parent::fieldLabels($includerelations);
		
		$labels['LockDay'] = _t('TradeLockPeriod.LOCK_DAY', 'Lock Day');
		$labels['Group'] = _t('TradeLockPeriod.GROUP', 'Group');
		$labels['GroupID'] = _t('TradeLockPeriod.GROUP', 'Group');
		$labels['Group.TitleByLang'] = _t('TradeLockPeriod.GROUP', 'Group');
		$labels['TradeSetting'] = _t('TradeLockPeriod.TRADE_SETTING', 'Trade Setting');
		$labels['TradeSettingID'] = _t('TradeLockPeriod.TRADE_SETTING', 'Trade Setting');
		
		return $labels;	
	}
	
	function getCMSFields() {
        $fields = parent::getCMSFields();
        $fields->dataFieldByName('GroupID')->setSource(Group::get()->filter('IsDistributorGroup', 1)->map('ID', 'TitleByLang')->toArray());

        return $fields;
    }

    function canView($member = false) {
        $extended = $this->extendedCan(__FUNCTION__, $member);
        if($extended !== null) {
            return $extended;
        }
        return Permission::check('VIEW_TradeLockPeriod');
    }

    function canEdit($member = false) {
        $extended = $this->extendedCan(__FUNCTION__, $member);
        if($extended !== null) {
            return $extended;
        }
        return Permission::check('EDIT_TradeLockPeriod');
    }

    function canDelete($member = false) {
        $extended = $this->extendedCan(__FUNCTION__, $member);
        if($extended !== null) {
            return $extended;
        }
        return Permission::check('DELETE_TradeLockPeriod');
    }

    function canCreate($member = false) {
        $extended = $this->extendedCan(__FUNCTION__, $member);
        if($extended !== null) {
            return $extended;
        }
        return Permission::check('CREATE_TradeLockPeriod');
    }

    public function providePermissions() {
        return array(
            'VIEW_TradeLockPeriod' => array(
                'name' => _t('TradeLockPeriod.PERMISSION_VIEW', 'Allow view access right'),
                'category' => _t('TradeLockPeriod.PERMISSIONS_CATEGORY', 'Trade Lock Period')
            ),
            'EDIT_TradeLockPeriod' => array(
                'name' => _t('TradeLockPeriod.PERMISSION_EDIT', 'Allow edit access right'),
                'category' => _t('TradeLockPeriod.PERMISSIONS_CATEGORY', 'Trade Lock Period')
            ),
            'DELETE_TradeLockPeriod' => array(
                'name' => _t('TradeLockPeriod.PERMISSION_DELETE', 'Allow delete access right'),
                'category' => _t('TradeLockPeriod.PERMISSIONS_CATEGORY', 'Trade Lock Period')
            ),
            'CREATE_TradeLockPeriod' => array(
                'name' => _t('TradeLockPeriod.PERMISSION_CREATE', 'Allow create access right'),
                'category' => _t('TradeLockPeriod.PERMISSIONS_CATEGORY', 'Trade Lock Period')
            )
        );
    }
}
?>