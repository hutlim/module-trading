<?php
/**
 * @package trading
 */
class TradeRequest extends DataObject implements PermissionProvider {
    private static $singular_name = "Trade Request";
    private static $plural_name = "Trade Requests";

    private static $db = array(
    	'Reference' => 'Varchar',
        'Unit' => 'Int',
        'Price' => 'TradeCurrency',
        'RequesterFee' => 'TradeCurrency',
        'ReceiverFee' => 'TradeCurrency',
        'Status' => "Enum(array('Pending', 'Cancelled', 'Accepted'))"
    );

    private static $has_one = array(
        'Requester' => 'Member',
        'Receiver' => 'Member',
        'TradeSetting' => 'TradeSetting',
        'ReservedShare' => 'ReservedShare'
    );
	
	private static $many_many = array(
		'CashAccounts' => 'CashAccount',
		'TradeCoinAccounts' => 'TradeCoinAccount',
		'TradePointAccounts' => 'TradePointAccount',
		'TradeHistories' => 'TradeHistory'
	);

    private static $default_sort = "Created DESC";

    private static $searchable_fields = array(
    	'Created' => array(
			'field' => 'DateField',
			'filter' => 'DateMatchFilter'
		),
    	'Requester.Username',
    	'Requester.FirstName',
    	'Requester.Surname',
    	'Receiver.Username',
    	'Receiver.FirstName',
    	'Receiver.Surname',
        'Unit' => array(
            'filter' => 'GreaterThanOrEqualFilter'
        ),
        'Price' => array(
            'filter' => 'GreaterThanOrEqualFilter'
        ),
        'Status'
    );

    private static $summary_fields = array(
    	'Requester.Username',
    	'Requester.Name',
    	'Receiver.Username',
    	'Receiver.Name',
    	'Created.Nice',
        'Unit',
        'Price',
        'RequesterFee',
        'ReceiverFee',
        'Status'
    );
	
	private static $casting = array(
		'Amount' => 'TradeCurrency'
	);
	
	/**
     * Generate reference for trade request
     * @return str Returns the reference
     */
    static function reference_generator() {
        $reference = rand(1, 99999999);
        $reference = str_pad($reference, 8, "0", STR_PAD_LEFT);
        while($result = TradeRequest::get()->filter('Reference', $reference)->count()) {
            $reference = rand(1, 999999999);
            $reference = str_pad($reference, 8, "0", STR_PAD_LEFT);
        }
        return $reference;
    }
	
	public function populateDefaults() {
        parent::populateDefaults();
		$this->Status = 'Pending';
    }

    public function fieldLabels($includerelations = true) {
        $labels = parent::fieldLabels($includerelations);

		$labels['Created'] = _t('TradeRequest.DATE', 'Date');
		$labels['Created.Nice'] = _t('TradeRequest.DATE', 'Date');
		$labels['Reference'] = _t('TradeRequest.REFERENCE', 'Reference');
        $labels['Unit'] = _t('TradeRequest.UNIT', 'Unit');
        $labels['Price'] = _t('TradeRequest.PRICE', 'Price');
		$labels['RequesterFee'] = _t('TradeRequest.REQUESTER_FEE', 'Requester Fee');
		$labels['ReceiverFee'] = _t('TradeRequest.RECEIVER_FEE', 'Receiver Fee');
		$labels['Status'] = _t('TradeRequest.STATUS', 'Status');
		$labels['TradeSetting'] = _t('TradeRequest.TRADE_SETTING', 'Trade Setting');
		$labels['TradeSettingID'] = _t('TradeRequest.TRADE_SETTING', 'Trade Setting');
		$labels['Requester.Username'] = _t('TradeRequest.USERNAME', 'Username');
		$labels['Requester.FirstName'] = _t('TradeRequest.FIRST_NAME', 'First Name');
		$labels['Requester.Surname'] = _t('TradeRequest.SURNAME', 'Surname');
		$labels['Requester.Name'] = _t('TradeRequest.NAME', 'Name');
		$labels['Receiver.Username'] = _t('TradeRequest.USERNAME', 'Username');
		$labels['Receiver.FirstName'] = _t('TradeRequest.FIRST_NAME', 'First Name');
		$labels['Receiver.Surname'] = _t('TradeRequest.SURNAME', 'Surname');
		$labels['Receiver.Name'] = _t('TradeRequest.NAME', 'Name');

        return $labels;
    }

    function validate() {
        $validationResult = parent::validate();

        if(!$this->RequesterID) {
            $subvalid = new ValidationResult();
            $subvalid->error(_t('TradeRequest.INVALID_REQUESTER_ID', 'Invalid Requester ID'), 'INVALID_REQUESTER_ID');
            $validationResult->combineAnd($subvalid);
        }
		
		if(!$this->ReceiverID) {
            $subvalid = new ValidationResult();
            $subvalid->error(_t('TradeRequest.INVALID_RECEIVER_ID', 'Invalid Receiver ID'), 'INVALID_RECEIVER_ID');
            $validationResult->combineAnd($subvalid);
        }

        if($this->Unit <= 0) {
            $subvalid = new ValidationResult();
            $subvalid->error(_t('TradeRequest.INVALID_TRADE_UNIT', 'Invalid trade unit'), 'INVALID_TRADE_UNIT');
            $validationResult->combineAnd($subvalid);
        }

        if($this->Price <= 0) {
            $subvalid = new ValidationResult();
            $subvalid->error(_t('TradeRequest.INVALID_TRADE_PRICE', 'Invalid trade price'), 'INVALID_TRADE_PRICE');
            $validationResult->combineAnd($subvalid);
        }
		
		if($this->RequesterFee < 0) {
            $subvalid = new ValidationResult();
            $subvalid->error(_t('TradeRequest.INVALID_REQUESTER_TRADE_FEE', 'Invalid requester trade fee'), 'INVALID_REQUESTER_TRADE_FEE');
            $validationResult->combineAnd($subvalid);
        }
		
		if($this->ReceiverFee < 0) {
            $subvalid = new ValidationResult();
            $subvalid->error(_t('TradeRequest.INVALID_RECEIVER_TRADE_FEE', 'Invalid receiver trade fee'), 'INVALID_RECEIVER_TRADE_FEE');
            $validationResult->combineAnd($subvalid);
        }
		
		if(!$this->ReservedShareID) {
            $subvalid = new ValidationResult();
            $subvalid->error(_t('TradeRequest.INVALID_RESERVED_SHARE_ID', 'Invalid reserved share id'), 'INVALID_RESERVED_SHARE_ID');
            $validationResult->combineAnd($subvalid);
        }
		else if(!$this->ReservedShare()->IsUnlock()){
			$subvalid = new ValidationResult();
            $subvalid->error(_t('TradeRequest.RESERVED_UNIT_IS_LOCKED', 'Reserved unit is locked'), 'RESERVED_SHARE_IS_LOCKED');
            $validationResult->combineAnd($subvalid);
		}

		if(!$this->TradeSettingID) {
            $subvalid = new ValidationResult();
            $subvalid->error(_t('TradeRequest.INVALID_TRADE_SETTING_ID', 'Invalid trade setting id'), 'INVALID_TRADE_SETTING_ID');
            $validationResult->combineAnd($subvalid);
        }
		else if(!$this->TradeSetting()->IsActive || !$this->TradeSetting()->IsStarted || $this->TradeSetting()->IsClosed){
			$subvalid = new ValidationResult();
            $subvalid->error(_t('TradeRequest.TRADE_MARKET_CLOSE', 'Sorry, currently trade market is closed'), 'TRADE_MARKET_CLOSE');
            $validationResult->combineAnd($subvalid);
		}
		
		if($this->isChanged('Status') && $this->Status == 'Accepted' && ($this->Amount + $this->ReceiverFee) > $this->Receiver()->TradeCoinAccountBalance){
			$subvalid = new ValidationResult();
            $subvalid->error(_t('TradeRequest.INSUFFICIENT_TRADE_COIN_BALANCE', "Insufficient Trade-Coin balance"), "INSUFFICIENT_TRADE_COIN_BALANCE");
            $validationResult->combineAnd($subvalid);
		}
		
		if(!$this->exists() && $this->Price > 0 && $this->Price != $this->TradeSetting()->Price){
			$subvalid = new ValidationResult();
            $subvalid->error(_t('TradeRequest.INVALID_TRADE_PRICE', 'Invalid trade price'), 'INVALID_TRADE_PRICE');
            $validationResult->combineAnd($subvalid);
		}

        return $validationResult;
    }

	function onBeforeWrite() {
        parent::onBeforeWrite();
        if($this->Reference == '') {
            $this->Reference = self::reference_generator();
        }
    }

	function onAfterWrite() {
        parent::onAfterWrite();
		
		if($this->isChanged('ID') && $this->Amount > 0){
			$this->ReservedShare()->Remaining -= $this->Amount;
			$this->ReservedShare()->write();
		}
		
        if($this->isChanged('Status') && $this->Status == 'Accepted' && $this->Amount > 0){
        	$trade_history = TradeHistory::create();
			$trade_history->Type = 'Sell';
			$trade_history->Reference = $this->Reference;
			$trade_history->Unit = $this->Unit;
			$trade_history->Price = $this->Price;
			$trade_history->Fee = $this->RequesterFee;
			$trade_history->MemberID = $this->RequesterID;
			$trade_history->TradeSettingID = $this->TradeSettingID;
			$trade_history->IsCompany = 1;
			$trade_history->write();
			$this->TradeHistories()->add($trade_history);
			
			$statement_data = array(
	            'Type' => 'Sell Share (Company)',
	            'Credit' => ($this->Amount - $this->RequesterFee) * 0.7,
	            'Reference' => $this->Reference,
	            'Description' => sprintf('Sell %s units of %s share at %s per unit. Fees %s, %s receivable.', $this->dbObject('Unit')->Formatted(), $this->Name, $this->dbObject('Price')->Nice(), $this->dbObject('RequesterFee')->Nice(), DBField::create_field('Percentage', 0.7)->Nice())
	        );
	    	$id = CashAccount::create_statement($statement_data, $this->RequesterID);
			$this->CashAccounts()->add($id);
			
			$statement_data = array(
	            'Type' => 'Sell Share (Company)',
	            'Credit' => ($this->Amount - $this->RequesterFee) * 0.3,
	            'Reference' => $this->Reference,
	            'Description' => sprintf('Sell %s units of %s share at %s per unit. Fees %s, %s receivable.', $this->dbObject('Unit')->Formatted(), $this->Name, $this->dbObject('Price')->Nice(), $this->dbObject('RequesterFee')->Nice(), DBField::create_field('Percentage', 0.3)->Nice())
	        );
	    	$id = TradePointAccount::create_statement($statement_data, $this->RequesterID);
			$this->TradePointAccounts()->add($id);
			
			
			$trade_history = TradeHistory::create();
			$trade_history->Type = 'Buy';
			$trade_history->Reference = $this->Reference;
			$trade_history->Unit = $this->Unit;
			$trade_history->Price = $this->Price;
			$trade_history->Fee = $this->ReceiverFee;
			$trade_history->MemberID = $this->ReceiverID;
			$trade_history->TradeSettingID = $this->TradeSettingID;
			$trade_history->write();
			$this->TradeHistories()->add($trade_history);
			
    		$statement_data = array(
                'Type' => 'Buy Share',
                'Debit' => $this->Amount + $this->ReceiverFee,
                'Reference' => $this->Reference,
                'Description' => sprintf('Buy order with %s units at price %s of share %s. Fees %s.', $this->dbObject('Unit')->Formatted(), $this->dbObject('Price')->Nice(), $this->Name, $this->obj('ReceiverFee')->Nice())
            );
        	$id = TradeCoinAccount::create_statement($statement_data, $this->ReceiverID);
			$this->TradeCoinAccounts()->add($id);
        }

		if($this->isChanged('Status') && $this->Status == 'Cancelled' && $this->Amount > 0){
			$this->ReservedShare()->Remaining += $this->Amount;
			$this->ReservedShare()->write();
		}
    }

	function getCancelAction(){
		if($this->Status == 'Pending'){
			$cancel_link = Controller::join_links(TradeRequestPage::get()->first()->Link('cancel'), $this->ID);
        	$action = sprintf('<a title="%s" class="btn btn-xs btn-primary" rel="tooltip" href="%s">%s</a>', _t('TradeRequest.CLICK_CANCEL_REQUEST', 'Click here to cancel sell {title} request', '', array('title' => $this->TradeSetting()->Title)), $cancel_link, _t('TradeRequest.BUTTONCANCEL', 'Cancel'));
			return $action;
		}
        return $this->Status;
	}

	function getAcceptAction(){
		if($this->Status == 'Pending'){
			$accept_link = Controller::join_links(TradeRequestPage::get()->first()->Link('accept'), $this->ID);
        	$action = sprintf('<a title="%s" class="btn btn-xs btn-primary" rel="tooltip" href="%s">%s</a>', _t('TradeRequest.CLICK_ACCEPT_REQUEST', 'Click here to accept buy {title} request', '', array('title' => $this->TradeSetting()->Title)), $accept_link, _t('TradeRequest.BUTTONACCEPT', 'Accept'));
			return $action;
		}
        return $this->Status;
	}
	
	function getName(){
		return $this->ReservedShare()->Name;
	}
	
	function getAmount(){
		return $this->Price * $this->Unit;
	}
	
	function canView($member = false) {
        $extended = $this->extendedCan(__FUNCTION__, $member);
        if($extended !== null) {
            return $extended;
        }
        return Permission::check('VIEW_TradeRequest');
    }

    function canEdit($member = false) {
        $extended = $this->extendedCan(__FUNCTION__, $member);
        if($extended !== null) {
            return $extended;
        }
        return Permission::check('EDIT_TradeRequest');
    }

    function canDelete($member = false) {
        $extended = $this->extendedCan(__FUNCTION__, $member);
        if($extended !== null) {
            return $extended;
        }
        return Permission::check('DELETE_TradeRequest');
    }

    function canCreate($member = false) {
        $extended = $this->extendedCan(__FUNCTION__, $member);
        if($extended !== null) {
            return $extended;
        }
        return Permission::check('CREATE_TradeRequest');
    }

    public function providePermissions() {
        return array(
            'VIEW_TradeRequest' => array(
                'name' => _t('TradeRequest.PERMISSION_VIEW', 'Allow view access right'),
                'category' => _t('TradeRequest.PERMISSIONS_CATEGORY', 'Trade Request')
            ),
            'EDIT_TradeRequest' => array(
                'name' => _t('TradeRequest.PERMISSION_EDIT', 'Allow edit access right'),
                'category' => _t('TradeRequest.PERMISSIONS_CATEGORY', 'Trade Request')
            ),
            'DELETE_TradeRequest' => array(
                'name' => _t('TradeRequest.PERMISSION_DELETE', 'Allow delete access right'),
                'category' => _t('TradeRequest.PERMISSIONS_CATEGORY', 'Trade Request')
            ),
            'CREATE_TradeRequest' => array(
                'name' => _t('TradeRequest.PERMISSION_CREATE', 'Allow create access right'),
                'category' => _t('TradeRequest.PERMISSIONS_CATEGORY', 'Trade Request')
            )
        );
    }
}
?>