<?php

class TradeCoinAccountAdjustment extends DataObject implements PermissionProvider {
    private static $singular_name = "Trade-Coin Adjustment";
    private static $plural_name = "Trade-Coin Adjustments";
    
    private static $extensions = array("AccountAdjustment");
	
	private static $db = array(
        'Amount' => 'TradeCurrency'
    );

    static function create_statement($data, $memberid) {
        if( !$memberid) {
            throw new Exception("Empty memberid");
        }
        return TradeCoinAccountAdjustment::create()
        ->update($data)
        ->setField('MemberID', $memberid)
        ->write();
    }
    
    function canView($member = false) {
        $extended = $this->extendedCan(__FUNCTION__, $member);
        if($extended !== null) {
            return $extended;
        }
        return Permission::check('VIEW_TradeCoinAccountAdjustment');
    }

    function canEdit($member = false) {
    	if(!$this->exists() && $this->canCreate($member)){
    		return true;
    	}
        return false;
    }

    function canDelete($member = false) {
        return false;
    }

    function canCreate($member = false) {
        $extended = $this->extendedCan(__FUNCTION__, $member);
        if($extended !== null) {
            return $extended;
        }
        return Permission::check('CREATE_TradeCoinAccountAdjustment');
    }

    public function providePermissions() {
        return array(
            'VIEW_TradeCoinAccountAdjustment' => array(
                'name' => _t('TradeCoinAccountAdjustment.PERMISSION_VIEW', 'Allow view access right'),
                'category' => _t('TradeCoinAccountAdjustment.PERMISSIONS_CATEGORY', 'Trade-Coin Account Adjustment')
            ),
            'CREATE_TradeCoinAccountAdjustment' => array(
                'name' => _t('TradeCoinAccountAdjustment.PERMISSION_CREATE', 'Allow create access right'),
                'category' => _t('TradeCoinAccountAdjustment.PERMISSIONS_CATEGORY', 'Trade-Coin Account Adjustment')
            )
        );
    }
}
?>