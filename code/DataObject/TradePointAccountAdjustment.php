<?php

class TradePointAccountAdjustment extends DataObject implements PermissionProvider {
    private static $singular_name = "Trade-Point Adjustment";
    private static $plural_name = "Trade-Point Adjustments";
    
    private static $extensions = array("AccountAdjustment");
	
	private static $db = array(
        'Amount' => 'TradeCurrency'
    );

    static function create_statement($data, $memberid) {
        if( !$memberid) {
            throw new Exception("Empty memberid");
        }
        return TradePointAccountAdjustment::create()
        ->update($data)
        ->setField('MemberID', $memberid)
        ->write();
    }
    
    function canView($member = false) {
        $extended = $this->extendedCan(__FUNCTION__, $member);
        if($extended !== null) {
            return $extended;
        }
        return Permission::check('VIEW_TradePointAccountAdjustment');
    }

    function canEdit($member = false) {
    	if(!$this->exists() && $this->canCreate($member)){
    		return true;
    	}
        return false;
    }

    function canDelete($member = false) {
        return false;
    }

    function canCreate($member = false) {
        $extended = $this->extendedCan(__FUNCTION__, $member);
        if($extended !== null) {
            return $extended;
        }
        return Permission::check('CREATE_TradePointAccountAdjustment');
    }

    public function providePermissions() {
        return array(
            'VIEW_TradePointAccountAdjustment' => array(
                'name' => _t('TradePointAccountAdjustment.PERMISSION_VIEW', 'Allow view access right'),
                'category' => _t('TradePointAccountAdjustment.PERMISSIONS_CATEGORY', 'Trade-Point Account Adjustment')
            ),
            'CREATE_TradePointAccountAdjustment' => array(
                'name' => _t('TradePointAccountAdjustment.PERMISSION_CREATE', 'Allow create access right'),
                'category' => _t('TradePointAccountAdjustment.PERMISSIONS_CATEGORY', 'Trade-Point Account Adjustment')
            )
        );
    }
}
?>