<?php

class BonusConvertTradePoint extends DataObject implements PermissionProvider {
    private static $singular_name = "E-Bonus Convert Trade-Point";
    private static $plural_name = "E-Bonus Convert Trade-Point";
    
    private static $extensions = array("AccountConvert");

	static function create_statement($data, $memberid){
		if(!$memberid) {
            throw new Exception("Empty memberid");
        }
		
        return BonusConvertTradePoint::create()->update($data)->setField('MemberID', $memberid)->write();
    }

    function canView($member = false) {
        $extended = $this->extendedCan(__FUNCTION__, $member);
        if($extended !== null) {
            return $extended;
        }
        return Permission::check('VIEW_BonusConvertTradePoint');
    }

    function canEdit($member = false) {
        return false;
    }

    function canDelete($member = false) {
        return false;
    }

    function canCreate($member = false) {
        return false;
    }

    public function providePermissions() {
        return array(
            'VIEW_BonusConvertTradePoint' => array(
                'name' => _t('BonusConvertTradePoint.PERMISSION_VIEW', 'Allow view access right'),
                'category' => _t('BonusConvertTradePoint.PERMISSIONS_CATEGORY', 'E-Bonus Convert Trade-Point')
            )
        );
    }
}
?>