<?php
class TradeCoinAccountType extends AccountType {
    private static $singular_name = "Trade-Coin Account Type";
    private static $plural_name = "Trade-Coin Account Type";
    
    private static $default_records = array(
        array(
            'Code' => 'Buy Share (Company)',
            'Title' => 'Buy Share (Company)',
            'Sort' => 10,
            'Locale' => 'en_US'
        ),
        array(
            'Code' => 'Sell Share (Company)',
            'Title' => 'Sell Share (Company)',
            'Sort' => 20,
            'Locale' => 'en_US'
        ),
        array(
            'Code' => 'Buy Share',
            'Title' => 'Buy Share',
            'Sort' => 30,
            'Locale' => 'en_US'
        ),
        array(
            'Code' => 'Sell Share',
            'Title' => 'Sell Share',
            'Sort' => 40,
            'Locale' => 'en_US'
        ),
        array(
            'Code' => 'Convert',
            'Title' => 'Convert',
            'Sort' => 50,
            'Locale' => 'en_US'
        ),
        array(
            'Code' => 'Adjustment',
            'Title' => 'Adjustment',
            'Sort' => 60,
            'Locale' => 'en_US'
        ),
        array(
            'Code' => 'Buy Share (Company)',
            'Title' => '购买股份（公司）',
            'Sort' => 70,
            'Locale' => 'zh_CN'
        ),
        array(
            'Code' => 'Sell Share (Company)',
            'Title' => '出售股份（公司）',
            'Sort' => 80,
            'Locale' => 'zh_CN'
        ),
        array(
            'Code' => 'Buy Share',
            'Title' => '购买股份',
            'Sort' => 90,
            'Locale' => 'zh_CN'
        ),
        array(
            'Code' => 'Sell Share',
            'Title' => '出售股份',
            'Sort' => 100,
            'Locale' => 'zh_CN'
        ),
        array(
            'Code' => 'Convert',
            'Title' => '余额兑换',
            'Sort' => 110,
            'Locale' => 'zh_CN'
        ),
        array(
            'Code' => 'Adjustment',
            'Title' => '余额调整',
            'Sort' => 120,
            'Locale' => 'zh_CN'
        )
    );

    /**
     *
     * @param Code
     * @return Title
     */
    static function get_title_by_code($code, $locale = null) {
    	if(!$locale) $locale = i18n::get_locale();
        if($type = TradeCoinAccountType::get()->filter('Active', 1)->filter('Locale', $locale)->find('Code', $code)) {
            return $type->Title;
        }
		return $code;
    }
}
?>