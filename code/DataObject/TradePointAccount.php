<?php

class TradePointAccount extends DataObject {
    private static $singular_name = "Trade-Point Account";
    private static $plural_name = "Trade-Point Accounts";

    private static $extensions = array("Account");
	
	private static $db = array(
        'Debit' => 'TradeCurrency',
        'Credit' => 'TradeCurrency',
        'ForwardBalance' => 'TradeCurrency'
    );
	
	private static $casting = array(
        'Balance' => 'TradeCurrency',
        'TotalDebit' => 'TradeCurrency',
        'TotalCredit' => 'TradeCurrency'
    );

    static function create_statement($data, $memberid) {
        if( !$memberid) {
            throw new Exception("Empty memberid");
        }
        return TradePointAccount::create()
        ->update($data)
        ->setField('MemberID', $memberid)
        ->write();
    }

    static function delete_statement($id) {
        if(!$id) {
            throw new Exception("Empty ID");
        }
        TradePointAccount::delete_by_id('TradePointAccount', $id);
    }

    function canView($member = false) {
        return true;
    }

    function canEdit($member = false) {
        return false;
    }

    function canDelete($member = false) {
        return false;
    }

    function canCreate($member = false) {
        return false;
    }
}
?>