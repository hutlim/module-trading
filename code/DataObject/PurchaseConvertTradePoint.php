<?php

class PurchaseConvertTradePoint extends DataObject implements PermissionProvider {
    private static $singular_name = "E-Purchase Convert Trade-Point";
    private static $plural_name = "E-Purchase Convert Trade-Point";
    
    private static $extensions = array("AccountConvert");

	static function create_statement($data, $memberid){
		if(!$memberid) {
            throw new Exception("Empty memberid");
        }
		
        return PurchaseConvertTradePoint::create()->update($data)->setField('MemberID', $memberid)->write();
    }

    function canView($member = false) {
        $extended = $this->extendedCan(__FUNCTION__, $member);
        if($extended !== null) {
            return $extended;
        }
        return Permission::check('VIEW_PurchaseConvertTradePoint');
    }

    function canEdit($member = false) {
        return false;
    }

    function canDelete($member = false) {
        return false;
    }

    function canCreate($member = false) {
        return false;
    }

    public function providePermissions() {
        return array(
            'VIEW_PurchaseConvertTradePoint' => array(
                'name' => _t('PurchaseConvertTradePoint.PERMISSION_VIEW', 'Allow view access right'),
                'category' => _t('PurchaseConvertTradePoint.PERMISSIONS_CATEGORY', 'E-Purchase Convert Trade-Point')
            )
        );
    }
}
?>