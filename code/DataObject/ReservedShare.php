<?php
/**
 * @package trading
 */
class ReservedShare extends DataObject implements PermissionProvider {
    private static $singular_name = "Reserved Share";
    private static $plural_name = "Reserved Shares";

    private static $db = array(
        'Amount' => 'TradeCurrency',
        'Remaining' => 'TradeCurrency',
        'LockPeriod' => 'Int'
    );

    private static $has_one = array(
        'Member' => 'Member',
        'PurchaseShare' => 'PurchaseShare',
        'TradeSetting' => 'TradeSetting'
    );
	
	private static $has_many = array(
		'TradeRequests' => 'TradeRequest'
	);

    private static $default_sort = "Created DESC";

    private static $searchable_fields = array(
    	'Created' => array(
			'field' => 'DateField',
			'filter' => 'DateMatchFilter'
		),
    	'Member.Username',
    	'Member.FirstName',
    	'Member.Surname',
    	'Amount' => array(
            'filter' => 'GreaterThanOrEqualFilter'
        ),
        'Remaining' => array(
            'filter' => 'GreaterThanOrEqualFilter'
        ),
		'LockPeriod' => array(
            'filter' => 'GreaterThanOrEqualFilter'
        )
    );

    private static $summary_fields = array(
    	'Member.Username',
    	'Member.Name',
    	'Created.Nice',
        'Amount',
        'Remaining',
        'LockPeriod'
    );
	
	private static $casting = array(
		'RemainLockPeriod' => 'Int'
	);

    public function fieldLabels($includerelations = true) {
        $labels = parent::fieldLabels($includerelations);

		$labels['Created'] = _t('ReservedShare.DATE', 'Date');
		$labels['Created.Nice'] = _t('ReservedShare.DATE', 'Date');
        $labels['Amount'] = _t('ReservedShare.AMOUNT', 'Amount');
        $labels['Remaining'] = _t('ReservedShare.REMAINING', 'Remaining');
		$labels['LockPeriod'] = _t('ReservedShare.LOCK_PERIOD', 'Lock Period');
		$labels['Member.Username'] = _t('ReservedShare.USERNAME', 'Username');
		$labels['Member.FirstName'] = _t('ReservedShare.FIRST_NAME', 'First Name');
		$labels['Member.Surname'] = _t('ReservedShare.SURNAME', 'Surname');
		$labels['Member.Name'] = _t('ReservedShare.NAME', 'Name');

        return $labels;
    }

    function validate() {
        $validationResult = parent::validate();

        if(!$this->MemberID) {
            $subvalid = new ValidationResult();
            $subvalid->error(_t('ReservedShare.INVALID_MEMBER_ID', 'Invalid Member ID'), 'INVALID_MEMBER_ID');
            $validationResult->combineAnd($subvalid);
        }

        if($this->Amount <= 0) {
            $subvalid = new ValidationResult();
            $subvalid->error(_t('ReservedShare.INVALID_RESERVED_AMOUNT', 'Invalid reserved amount'), 'INVALID_RESERVED_AMOUNT');
            $validationResult->combineAnd($subvalid);
        }

        if($this->Remaining <= 0) {
            $subvalid = new ValidationResult();
            $subvalid->error(_t('ReservedShare.INVALID_REMAINING_RESERVED_AMOUNT', 'Invalid remaining reserved amount'), 'INVALID_REMAINING_RESERVED_AMOUNT');
            $validationResult->combineAnd($subvalid);
        }
		
		if($this->LockPeriod < 0) {
            $subvalid = new ValidationResult();
            $subvalid->error(_t('ReservedShare.INVALID_LOCK_PERIOD', 'Invalid lock period'), 'INVALID_LOCK_PERIOD');
            $validationResult->combineAnd($subvalid);
        }

		if(!$this->PurchaseShareID) {
            $subvalid = new ValidationResult();
            $subvalid->error(_t('ReservedShare.INVALID_PURCHASE_SHARE_ID', 'Invalid purchase share id'), 'INVALID_PURCHASE_SHARE_ID');
            $validationResult->combineAnd($subvalid);
        }

		if(!$this->TradeSettingID) {
            $subvalid = new ValidationResult();
            $subvalid->error(_t('ReservedShare.INVALID_TRADE_SETTING_ID', 'Invalid trade setting id'), 'INVALID_TRADE_SETTING_ID');
            $validationResult->combineAnd($subvalid);
        }
		else if(!$this->TradeSetting()->IsActive || !$this->TradeSetting()->IsStarted || $this->TradeSetting()->IsClosed){
			$subvalid = new ValidationResult();
            $subvalid->error(_t('ReservedShare.TRADE_MARKET_CLOSE', 'Sorry, currently trade market is closed'), 'TRADE_MARKET_CLOSE');
            $validationResult->combineAnd($subvalid);
		}

        return $validationResult;
    }

	function getAction(){
		if($this->IsUnlock()){
			$request_action = '';
			if($this->Remaining){
		        $sell_link = Controller::join_links(ReservedSharePage::get()->first()->Link('request'), $this->ID);
		        $request_action = sprintf('<li><a title="%s" data-title="%s" rel="tooltip popup" href="%s">%s</a></li>', _t('ReservedShare.CLICK_REQUEST_SELL', 'Click here to submit request sell'), _t('ReservedShare.REQUEST_SELL_SHARE', 'Request Sell {title}', '', array('title' => $this->TradeSetting()->Title)), $sell_link, _t('ReservedShare.BUTTONSENDREQUEST', 'Send Request'));
			}

			$view_link = sprintf('%s?%s=%s', ReservedSharePage::get()->first()->Link('view'), 'ID', $this->ID);
			$action = sprintf('<div class="btn-group"><button type="button" class="btn btn-default btn-xs dropdown-toggle" data-toggle="dropdown">Action <span class="caret"></span></button><ul class="dropdown-menu dropdown-menu-right"><li><a title="%s" data-title="%s" rel="popup tooltip" data-modal-lg="1" href="%s">%s</a></li>%s</ul></div>', _t('ReservedShare.CLICK_VIEW_REQUEST', 'Click here to view sell request'), _t('ReservedShare.SELL_SHARE_REQUEST', 'Sell {title} Request', '', array('title' => $this->TradeSetting()->Title)), $view_link, _t('ReservedShare.BUTTONVIEW', 'View Request'), $request_action);
			
			return $action;
		}
		
		return _t('ReservedShare.LOCKED', 'Locked for {lock_period} days', '', array('lock_period' => $this->RemainLockPeriod));
    }
	
	function IsUnlock(){
		$time = strtotime(sprintf('%s + %s days', $this->dbObject('Created')->URLDate(), $this->LockPeriod));
		return (time() >= $time);
	}

	function getRemainLockPeriod(){
		$locktime = strtotime(sprintf('%s + %s days', $this->dbObject('Created')->URLDate(), $this->LockPeriod));
		$date = new Date();
		return $date->days_between(date('Y', $locktime), date('m', $locktime), date('d', $locktime), date('Y'), date('m'), date('d'));
	}
	
	function getName(){
		return $this->PurchaseShare()->Name;
	}
	
	function canView($member = false) {
        $extended = $this->extendedCan(__FUNCTION__, $member);
        if($extended !== null) {
            return $extended;
        }
        return Permission::check('VIEW_ReservedShare');
    }

    function canEdit($member = false) {
        return false;
    }

    function canDelete($member = false) {
        return false;
    }

    function canCreate($member = false) {
        return false;
    }

    public function providePermissions() {
        return array(
            'VIEW_ReservedShare' => array(
                'name' => _t('ReservedShare.PERMISSION_VIEW', 'Allow view access right'),
                'category' => _t('ReservedShare.PERMISSIONS_CATEGORY', 'Reserved Share')
            )
        );
    }
}
?>