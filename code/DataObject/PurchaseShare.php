<?php
/**
 * @package trading
 */
class PurchaseShare extends DataObject implements PermissionProvider {
    private static $singular_name = "Purchase Share";
    private static $plural_name = "Purchase Shares";

    private static $db = array(
        'Reference' => 'Varchar',
        'Unit' => 'Int',
        'Price' => 'TradeCurrency',
        'FeePercentage' => 'Percentage'
    );

    private static $has_one = array(
        'Member' => 'Member',
        'TradeSetting' => 'TradeSetting'
    );
	
	private static $many_many = array(
		'TradePointAccounts' => 'TradePointAccount',
		'TradeHistories' => 'TradeHistory'
	);

    private static $default_sort = "Created DESC";

    private static $searchable_fields = array(
    	'Created' => array(
			'field' => 'DateField',
			'filter' => 'DateMatchFilter'
		),
    	'Member.Username',
    	'Member.FirstName',
    	'Member.Surname',
        'Reference',
        'Unit' => array(
            'filter' => 'GreaterThanOrEqualFilter'
        ),
        'Price' => array(
            'filter' => 'GreaterThanOrEqualFilter'
        )
    );

    private static $summary_fields = array(
    	'Member.Username',
    	'Member.Name',
    	'Created.Nice',
        'Reference',
        'Unit',
        'Price',
        'Fee'
    );
	
	private static $casting = array(
		'Amount' => 'TradeCurrency',
		'Fee' => 'TradeCurrency'
	);
	
	/**
     * Generate reference for purchase share
     * @return str Returns the reference
     */
    static function reference_generator() {
        $reference = rand(1, 99999999);
        $reference = str_pad($reference, 8, "0", STR_PAD_LEFT);
        while($result = PurchaseShare::get()->filter('Reference', $reference)->count()) {
            $reference = rand(1, 999999999);
            $reference = str_pad($reference, 8, "0", STR_PAD_LEFT);
        }
        return $reference;
    }

    public function fieldLabels($includerelations = true) {
        $labels = parent::fieldLabels($includerelations);

		$labels['Created'] = _t('PurchaseShare.DATE', 'Date');
		$labels['Created.Nice'] = _t('PurchaseShare.DATE', 'Date');
		$labels['Reference'] = _t('PurchaseShare.REFERENCE', 'Reference');
        $labels['Unit'] = _t('PurchaseShare.UNIT', 'Unit');
        $labels['Price'] = _t('PurchaseShare.PRICE', 'Price');
		$labels['Fee'] = _t('PurchaseShare.FEE', 'Fee');
		$labels['FeePercentage'] = _t('PurchaseShare.FEE_PERCENTAGE', 'Fee Percentage');
		$labels['TradeSetting'] = _t('PurchaseShare.TRADE_SETTING', 'Trade Setting');
		$labels['TradeSettingID'] = _t('PurchaseShare.TRADE_SETTING', 'Trade Setting');
		$labels['Member.Username'] = _t('PurchaseShare.USERNAME', 'Username');
		$labels['Member.FirstName'] = _t('PurchaseShare.FIRST_NAME', 'First Name');
		$labels['Member.Surname'] = _t('PurchaseShare.SURNAME', 'Surname');
		$labels['Member.Name'] = _t('PurchaseShare.NAME', 'Name');

        return $labels;
    }

    function validate() {
        $validationResult = parent::validate();

        if(!$this->MemberID) {
            $subvalid = new ValidationResult();
            $subvalid->error(_t('PurchaseShare.INVALID_MEMBER_ID', 'Invalid Member ID'), 'INVALID_MEMBER_ID');
            $validationResult->combineAnd($subvalid);
        } else { 
        	if(!$this->exists() && ($this->Amount + $this->Fee) > $this->Member()->TradePointAccountBalance) {
	            $subvalid = new ValidationResult();
	            $subvalid->error(_t('PurchaseShare.INSUFFICIENT_TRADE_POINT_BALANCE', "Insufficient Trade-Point balance"), "INSUFFICIENT_TRADE_POINT_BALANCE");
	            $validationResult->combineAnd($subvalid);
            }
        }

        if($this->Unit <= 0) {
            $subvalid = new ValidationResult();
            $subvalid->error(_t('PurchaseShare.INVALID_TRADE_UNIT', 'Invalid trade unit'), 'INVALID_TRADE_UNIT');
            $validationResult->combineAnd($subvalid);
        }

        if($this->Price <= 0) {
            $subvalid = new ValidationResult();
            $subvalid->error(_t('PurchaseShare.INVALID_TRADE_PRICE', 'Invalid trade price'), 'INVALID_TRADE_PRICE');
            $validationResult->combineAnd($subvalid);
        }
		
		if($this->FeePercentage < 0) {
            $subvalid = new ValidationResult();
            $subvalid->error(_t('PurchaseShare.INVALID_TRADE_FEE_PERCENTAGE', 'Invalid trade fee percentage'), 'INVALID_TRADE_FEE_PERCENTAGE');
            $validationResult->combineAnd($subvalid);
        }

		if(!$this->TradeSettingID) {
            $subvalid = new ValidationResult();
            $subvalid->error(_t('PurchaseShare.INVALID_TRADE_SETTING_ID', 'Invalid trade setting id'), 'INVALID_TRADE_SETTING_ID');
            $validationResult->combineAnd($subvalid);
        }
		else if(!$this->TradeSetting()->IsActive || !$this->TradeSetting()->IsStarted || $this->TradeSetting()->IsClosed){
			$subvalid = new ValidationResult();
            $subvalid->error(_t('PurchaseShare.TRADE_MARKET_CLOSE', 'Sorry, currently trade market is closed'), 'TRADE_MARKET_CLOSE');
            $validationResult->combineAnd($subvalid);
		}
		
		if(!$this->exists() && $this->Price > 0 && $this->Price != $this->TradeSetting()->Price){
			$subvalid = new ValidationResult();
            $subvalid->error(_t('PurchaseShare.INVALID_TRADE_PRICE', 'Invalid trade price'), 'INVALID_TRADE_PRICE');
            $validationResult->combineAnd($subvalid);
		}
		
		if(!$this->exists() && $this->FeePercentage > 0 && $this->FeePercentage != $this->TradeSetting()->BuyFeePercentage){
			$subvalid = new ValidationResult();
            $subvalid->error(_t('PurchaseShare.INVALID_TRADE_FEE_PERCENTAGE', 'Invalid trade fee percentage'), 'INVALID_TRADE_FEE_PERCENTAGE');
            $validationResult->combineAnd($subvalid);
		}

        return $validationResult;
    }

	function onBeforeWrite() {
        parent::onBeforeWrite();
        if($this->Reference == '') {
            $this->Reference = self::reference_generator();
        }
    }

    function onAfterWrite() {
        parent::onAfterWrite();
		
        if($this->isChanged('ID')){
        	if($this->Amount > 0){
        		$trade_history = TradeHistory::create();
				$trade_history->Type = 'Buy';
				$trade_history->Reference = $this->Reference;
				$trade_history->Unit = $this->Unit;
				$trade_history->Price = $this->Price;
				$trade_history->Fee = $this->FeePercentage * $this->Unit * $this->Price;
				$trade_history->IsCompany = 1;
				$trade_history->MemberID = $this->MemberID;
				$trade_history->TradeSettingID = $this->TradeSettingID;
				$trade_history->write();
				$this->TradeHistories()->add($trade_history);
				
        		$statement_data = array(
	                'Type' => 'Buy Share (Company)',
	                'Debit' => $this->Amount + $this->Fee,
	                'Reference' => $this->Reference,
	                'Description' => sprintf('Buy %s units at price %s of share %s. Fees %s.', $this->dbObject('Unit')->Formatted(), $this->dbObject('Price')->Nice(), $this->Name, $this->obj('Fee')->Nice())
	            );
	        	$id = TradePointAccount::create_statement($statement_data, $this->MemberID);
				$this->TradePointAccounts()->add($id);
				
				$reserved_share = ReservedShare::create();
				$reserved_share->Amount = $this->Amount;
				$reserved_share->Remaining = $this->Amount;
				$lock = $this->TradeSetting()->LockPeriods()->find('GroupID', $this->Member()->RankID);
				$reserved_share->LockPeriod = $lock ? $lock->LockDay : 0;
				$reserved_share->MemberID = $this->MemberID;
				$reserved_share->PurchaseShareID = $this->ID;
				$reserved_share->TradeSettingID = $this->TradeSettingID;
				$reserved_share->write();
			}
        }
    }
	
	function getName(){
		return $this->TradeSetting()->Title;
	}
	
	function getAmount(){
		return $this->Price * $this->Unit;
	}
	
	function getFee(){
		return $this->Amount * $this->FeePercentage;
	}
	
	function canView($member = false) {
        $extended = $this->extendedCan(__FUNCTION__, $member);
        if($extended !== null) {
            return $extended;
        }
        return Permission::check('VIEW_PurchaseShare');
    }

    function canEdit($member = false) {
        return false;
    }

    function canDelete($member = false) {
        return false;
    }

    function canCreate($member = false) {
        $extended = $this->extendedCan(__FUNCTION__, $member);
        if($extended !== null) {
            return $extended;
        }
        return Permission::check('CREATE_PurchaseShare');
    }

    public function providePermissions() {
        return array(
            'VIEW_PurchaseShare' => array(
                'name' => _t('PurchaseShare.PERMISSION_VIEW', 'Allow view access right'),
                'category' => _t('PurchaseShare.PERMISSIONS_CATEGORY', 'Purchase Share')
            ),
            'CREATE_PurchaseShare' => array(
                'name' => _t('PurchaseShare.PERMISSION_CREATE', 'Allow create access right'),
                'category' => _t('PurchaseShare.PERMISSIONS_CATEGORY', 'Purchase Share')
            )
        );
    }
}
?>