<?php
/**
 * Trading administration interface, based on ModelAdmin
 * @package trading
 */
class TradingAdmin extends CustomModelAdmin {

    private static $url_segment = 'trading';
    private static $menu_title = 'Trading';
    private static $menu_icon = 'trading/images/trading-icon.png';

    private static $managed_models = array(
    	'TradeSetting',
    	'TradePointAccountAdjustment',
    	'TradeCoinAccountAdjustment',
    	'TradeOrder',
    	'TradeHistory',
    	'CompanyTradeRequest'
    );
	
	public $showImportForm = false;
	
	public function getEditForm($id = null, $fields = null) {
        $list = $this->getList();
        if(ClassInfo::exists('GridFieldExportToExcelButton')){
        	$exportButton = new GridFieldExportToExcelButton('buttons-after-left');
		}
		else{
			$exportButton = new GridFieldExportButton('buttons-after-left');
		}
        $exportButton->setExportColumns($this->getExportFields());
        
		if(preg_match("/adjustment/i", $this->modelClass)){
            $listField = GridField::create(
                $this->sanitiseClassName($this->modelClass),
                false,
                $list,
                $fieldConfig = GridFieldConfig_RecordEditor::create($this->stat('page_length'))
                    ->removeComponentsByType('GridFieldFilterHeader')
					->removeComponentsByType('GridFieldEditButton')
                    ->addComponents(new GridFieldButtonRow('after'), new GridFieldPrintButton('buttons-after-left'), $exportButton)
            );
        }
		else if($this->modelClass == 'TradeOrder' || $this->modelClass == 'TradeHistory' || $this->modelClass == 'CompanyTradeRequest') {
	        $listField = GridField::create(
	            $this->sanitiseClassName($this->modelClass),
	            false,
	            $list,
	            $fieldConfig = GridFieldConfig_RecordEditor::create($this->stat('page_length'))
	                ->removeComponentsByType('GridFieldFilterHeader')
	                ->removeComponentsByType('GridFieldEditButton')
	                ->addComponents(new GridFieldViewButton(), new GridFieldButtonRow('after'), new GridFieldPrintButton('buttons-after-left'), $exportButton)
	        );
		}
		else {
	        $listField = GridField::create(
	            $this->sanitiseClassName($this->modelClass),
	            false,
	            $list,
	            $fieldConfig = GridFieldConfig_RecordEditor::create($this->stat('page_length'))
	                ->removeComponentsByType('GridFieldFilterHeader')
	                ->addComponents(new GridFieldButtonRow('after'), new GridFieldPrintButton('buttons-after-left'), $exportButton)
	        );
		}
        
        // Validation
        if(singleton($this->modelClass)->hasMethod('getCMSValidator')) {
            $detailValidator = singleton($this->modelClass)->getCMSValidator();
            $listField->getConfig()->getComponentByType('GridFieldDetailForm')->setValidator($detailValidator);
        }

        $form = CMSForm::create( 
            $this,
            'EditForm',
            new FieldList($listField),
            new FieldList()
        )->setHTMLID('Form_EditForm');
		$form->setResponseNegotiator($this->getResponseNegotiator());
        $form->addExtraClass('cms-edit-form cms-panel-padded center');
        $form->setTemplate($this->getTemplatesWithSuffix('_EditForm'));
        $editFormAction = Controller::join_links($this->Link($this->sanitiseClassName($this->modelClass)), 'EditForm');
        $form->setFormAction($editFormAction);
        $form->setAttribute('data-pjax-fragment', 'CurrentForm');

        $this->extend('updateEditForm', $form);
        
        return $form;
    }
}
?>