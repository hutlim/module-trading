<?php

/**
 * @package trading
 */

class TradePointAccountBalanceReport extends CustomReport {
	protected $sort = 70;
	
	public function title() {
		return _t('TradePointAccountBalanceReport.TITLE', 'Trade-Point Account Balance Report');
	}
	
	public function getCMSFields() {
		$fields = parent::getCMSFields();

		$fields->dataFieldByName('Report')->setModelClass('TradePointAccount');
		
		return $fields;
	}
	
	public function sourceRecords($params, $sort, $limit) {
		if($sort) {
			$parts = explode(' ', $sort);
			$field = $parts[0];
			$direction = $parts[1];
			
			if($field == 'Name') {
			    $sort = 'FirstName ' . $direction;
			}
		}
        
		$where = array('Member.IsDistributor = 1');
        if(isset($params['StartBalance']) && is_numeric($params['StartBalance'])){
        	$where[] = sprintf("Balance >= %s", (int)$params['StartBalance']);
        }
		
		if(isset($params['EndBalance']) && is_numeric($params['EndBalance'])){
        	$where[] = sprintf("Balance <= %s", (int)$params['EndBalance']);
        }
        
        if(isset($params['Username']) && $params['Username']){
        	$where[] = sprintf("Username LIKE '%%%s%%'", $params['Username']);
        }
		
		$query = new SQLQuery();
		$query = $query->setFrom('TradePointAccountBalance')->setSelect(array('MemberID'))->setWhere($where)->setOrderBy($sort)->setLimit($limit)->addInnerJoin('Member', 'TradePointAccountBalance.MemberID = Member.ID')->selectField('TradePointAccountBalance.TotalDebit', 'Debit')->selectField('TradePointAccountBalance.TotalCredit', 'Credit')->selectField('TradePointAccountBalance.Balance', 'ForwardBalance');
		
		$returnSet = new ArrayList();
		foreach($query->execute() as $item){
			$data = TradePointAccount::create()->update($item);
			$returnSet->push($data);
		}

		return $returnSet;
	}

	public function columns() {
		$fields = array(
			'Member.Username' => array(
				'title' => _t('TradePointAccountBalanceReport.USERNAME', 'Username')
			),
			'Member.Name' => array(
				'title' => _t('TradePointAccountBalanceReport.NAME', 'Name')
			),
			'Credit' => array(
                'title' => _t('TradePointAccountBalanceReport.TOTAL_IN', 'Total In'),
                'casting' => 'Decimal->Nice'
            ),
            'Debit' => array(
                'title' => _t('TradePointAccountBalanceReport.TOTAL_OUT', 'Total Out'),
                'casting' => 'Decimal->Nice'
            ),
            'ForwardBalance' => array(
                'title' => _t('TradePointAccountBalanceReport.TOTAL_BALANCE', 'Total Balance'),
                'casting' => 'Decimal->Nice'
            )
		);
		
		return $fields;
	}
	
	public function parameterFields() {
		return new FieldList(
            FieldGroup::create(
            	NumericField::create('filters[StartBalance]', '')->addExtraClass('no-change-track'),
				NumericField::create('filters[EndBalance]', '')->addExtraClass('no-change-track')
			)->setTitle(_t('TradePointAccountBalanceReport.TOTAL_BALANCE', 'Total Balance')),
			UsernameField::create('Username', _t('TradePointAccountBalanceReport.USERNAME', 'Username'))
		);
	}
}
