<?php

/**
 * @package trading
 */

class TradePointAccountSummaryReport extends CustomReport {
	protected $sort = 70;
	
	public function title() {
		return _t('TradePointAccountSummaryReport.TITLE', 'Trade-Point Transaction Summary Report');
	}
	
	public function getCMSFields() {
		$fields = parent::getCMSFields();

		$fields->dataFieldByName('Report')->setModelClass('TradePointAccount');
		
		return $fields;
	}
	
	public function sourceRecords($params, $sort, $limit) {
		if($sort) {
			$parts = explode(' ', $sort);
			$field = $parts[0];
			$direction = $parts[1];
			
			if($field == 'Name') {
			    $sort = 'FirstName ' . $direction;
			}
		}

		$where = array('Member.IsDistributor = 1');
        if(isset($params['StartDate']) && $params['StartDate']){
        	$where[] = sprintf("DATE(TradePointAccount.Date) >= '%s'", DBField::create_field('Date', $params['StartDate'])->URLDate());
        }
		
		if(isset($params['EndDate']) && $params['EndDate']){
        	$where[] = sprintf("DATE(TradePointAccount.Date) <= '%s'", DBField::create_field('Date', $params['EndDate'])->URLDate());
        }
        
        if(isset($params['Username']) && $params['Username']){
        	$where[] = sprintf("Member.Username LIKE '%%%s%%'", $params['Username']);
        }
		
		$query = new SQLQuery();
		$query = $query->setFrom('TradePointAccount')->setSelect(array('MemberID'))->setWhere($where)->setOrderBy($sort)->setLimit($limit)->setGroupBy('TradePointAccount.MemberID')->addInnerJoin('Member', 'TradePointAccount.MemberID = Member.ID')->selectField('SUM(TradePointAccount.Debit)', 'Debit')->selectField('SUM(TradePointAccount.Credit)', 'Credit')->selectField('SUM(TradePointAccount.Credit - TradePointAccount.Debit)', 'ForwardBalance');

		$returnSet = new ArrayList();
		foreach($query->execute() as $item){
			$data = TradePointAccount::create()->update($item);
			$returnSet->push($data);
		}

		return $returnSet;
	}

	public function columns() {
		$fields = array(
			'Member.Username' => array(
				'title' => _t('TradePointAccountSummaryReport.USERNAME', 'Username')
			),
			'Member.Name' => array(
				'title' => _t('TradePointAccountSummaryReport.NAME', 'Name')
			),
			'Credit' => array(
                'title' => _t('TradePointAccountSummaryReport.IN', 'In'),
                'casting' => 'Decimal->Nice'
            ),
            'Debit' => array(
                'title' => _t('TradePointAccountSummaryReport.OUT', 'Out'),
                'casting' => 'Decimal->Nice'
            ),
            'ForwardBalance' => array(
                'title' => _t('TradePointAccountSummaryReport.BALANCE', 'Balance'),
                'casting' => 'Decimal->Nice'
            )
		);
		
		return $fields;
	}
	
	public function parameterFields() {
		return new FieldList(
            FieldGroup::create(
            	DateField::create('filters[StartDate]', '')->addExtraClass('no-change-track'),
				DateField::create('filters[EndDate]', '')->addExtraClass('no-change-track')
			)->setTitle(_t('TradePointAccountSummaryReport.DATE', 'Date')),
			UsernameField::create('Username', _t('TradePointAccountSummaryReport.USERNAME', 'Username'))
		);
	}
}
