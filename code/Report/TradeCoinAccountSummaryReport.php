<?php

/**
 * @package trading
 */

class TradeCoinAccountSummaryReport extends CustomReport {
	protected $sort = 60;
	
	public function title() {
		return _t('TradeCoinAccountSummaryReport.TITLE', 'Trade-Coin Transaction Summary Report');
	}
	
	public function getCMSFields() {
		$fields = parent::getCMSFields();

		$fields->dataFieldByName('Report')->setModelClass('TradeCoinAccount');
		
		return $fields;
	}
	
	public function sourceRecords($params, $sort, $limit) {
		if($sort) {
			$parts = explode(' ', $sort);
			$field = $parts[0];
			$direction = $parts[1];
			
			if($field == 'Name') {
			    $sort = 'FirstName ' . $direction;
			}
		}

		$where = array('Member.IsDistributor = 1');
        if(isset($params['StartDate']) && $params['StartDate']){
        	$where[] = sprintf("DATE(TradeCoinAccount.Date) >= '%s'", DBField::create_field('Date', $params['StartDate'])->URLDate());
        }
		
		if(isset($params['EndDate']) && $params['EndDate']){
        	$where[] = sprintf("DATE(TradeCoinAccount.Date) <= '%s'", DBField::create_field('Date', $params['EndDate'])->URLDate());
        }
        
        if(isset($params['Username']) && $params['Username']){
        	$where[] = sprintf("Member.Username LIKE '%%%s%%'", $params['Username']);
        }
		
		$query = new SQLQuery();
		$query = $query->setFrom('TradeCoinAccount')->setSelect(array('MemberID'))->setWhere($where)->setOrderBy($sort)->setLimit($limit)->setGroupBy('TradeCoinAccount.MemberID')->addInnerJoin('Member', 'TradeCoinAccount.MemberID = Member.ID')->selectField('SUM(TradeCoinAccount.Debit)', 'Debit')->selectField('SUM(TradeCoinAccount.Credit)', 'Credit')->selectField('SUM(TradeCoinAccount.Credit - TradeCoinAccount.Debit)', 'ForwardBalance');

		$returnSet = new ArrayList();
		foreach($query->execute() as $item){
			$data = TradeCoinAccount::create()->update($item);
			$returnSet->push($data);
		}

		return $returnSet;
	}

	public function columns() {
		$fields = array(
			'Member.Username' => array(
				'title' => _t('TradeCoinAccountSummaryReport.USERNAME', 'Username')
			),
			'Member.Name' => array(
				'title' => _t('TradeCoinAccountSummaryReport.NAME', 'Name')
			),
			'Credit' => array(
                'title' => _t('TradeCoinAccountSummaryReport.IN', 'In'),
                'casting' => 'Decimal->Nice'
            ),
            'Debit' => array(
                'title' => _t('TradeCoinAccountSummaryReport.OUT', 'Out'),
                'casting' => 'Decimal->Nice'
            ),
            'ForwardBalance' => array(
                'title' => _t('TradeCoinAccountSummaryReport.BALANCE', 'Balance'),
                'casting' => 'Decimal->Nice'
            )
		);
		
		return $fields;
	}
	
	public function parameterFields() {
		return new FieldList(
            FieldGroup::create(
            	DateField::create('filters[StartDate]', '')->addExtraClass('no-change-track'),
				DateField::create('filters[EndDate]', '')->addExtraClass('no-change-track')
			)->setTitle(_t('TradeCoinAccountSummaryReport.DATE', 'Date')),
			UsernameField::create('Username', _t('TradeCoinAccountSummaryReport.USERNAME', 'Username'))
		);
	}
}
