<?php

/**
 * @package trading
 */

class TradeCoinAccountHistoryReport extends CustomReport {
	protected $sort = 60;
	
	public function title() {
		return _t('TradeCoinAccountHistoryReport.TITLE', 'Trade-Coin Transaction History Report');
	}
	
	public function getCMSFields() {
		$fields = parent::getCMSFields();

		$fields->dataFieldByName('Report')->setModelClass('TradeCoinAccount');
		
		return $fields;
	}
	
	public function sourceRecords($params, $sort, $limit) {
		if($sort) {
			$parts = explode(' ', $sort);
			$field = $parts[0];
			$direction = $parts[1];
			
			if($field == 'Name') {
			    $sort = 'FirstName ' . $direction;
			}
		}
        
        $ret = TradeCoinAccount::get()->sort($sort)->limit($limit);

        if(isset($params['Type']) && $params['Type']){
            $ret = $ret->filter('Type', $params['Type']);
        }

		if(isset($params['Submit']) && $params['Submit']){
	        if(isset($params['StartDate']) && $params['StartDate']){
	        	$ret = $ret->filter('Date:GreaterThanOrEqual', DBField::create_field('Date', $params['StartDate'])->URLDate() . '00:00:00');
	        }
		}
		else{
			$ret = $ret->filter('Date:GreaterThanOrEqual', date('Y-m-01 00:00:00'));
		}
		
		if(isset($params['Submit']) && $params['Submit']){
			if(isset($params['EndDate']) && $params['EndDate']){
	        	$ret = $ret->filter('Date:LessThanOrEqual', DBField::create_field('Date', $params['EndDate'])->URLDate() . '23:59:59');
	        }
		}
		else{
			$ret = $ret->filter('Date:LessThanOrEqual', date('Y-m-t 23:59:59'));
		}
        
        if(isset($params['Username']) && $params['Username']){
        	$ret = $ret->filter('Member.Username:PartialMatch', $params['Username']);
        }
        
        if(isset($params['Reference']) && $params['Reference']){
        	$ret = $ret->filter('Reference:PartialMatch', $params['Reference']);
        }

		return $ret;
	}

	public function columns() {
		$fields = array(
            'Date' => array(
                'title' => _t('TradeCoinAccountHistoryReport.DATE', 'Date'),
                'casting' => 'SS_Datetime->Nice'
            ),
            'TypeByLang' => array(
                'title' => _t('TradeCoinAccountHistoryReport.TYPE', 'Type')
            ),
			'Member.Username' => array(
				'title' => _t('TradeCoinAccountHistoryReport.USERNAME', 'Username')
			),
			'Member.Name' => array(
				'title' => _t('TradeCoinAccountHistoryReport.NAME', 'Name')
			),
			'Reference' => array(
				'title' => _t('TradeCoinAccountHistoryReport.REFERENCE', 'Reference')
			),
			'Description' => array(
				'title' => _t('TradeCoinAccountHistoryReport.DESCRIPTION', 'Description')
			),
			'Credit' => array(
                'title' => _t('TradeCoinAccountHistoryReport.IN', 'In'),
                'casting' => 'Decimal->Nice'
            ),
            'Debit' => array(
                'title' => _t('TradeCoinAccountHistoryReport.OUT', 'Out'),
                'casting' => 'Decimal->Nice'
            ),
            'ForwardBalance' => array(
                'title' => _t('TradeCoinAccountHistoryReport.BALANCE', 'Balance'),
                'casting' => 'Decimal->Nice'
            )
		);
		
		return $fields;
	}
	
	public function parameterFields() {
		return new FieldList(
			singleton('TradeCoinAccountType')->getDropdownField('Type', _t('TradeCoinAccountHistoryReport.TYPE', 'Type'))->setEmptyString(_t('TradeCoinAccountHistoryReport.ALL', 'All')),
            FieldGroup::create(
            	DateField::create('filters[StartDate]', '', date('Y-m-01'))->addExtraClass('no-change-track'),
				DateField::create('filters[EndDate]', '', date('Y-m-t'))->addExtraClass('no-change-track')
			)->setTitle(_t('TradeCoinAccountHistoryReport.DATE', 'Date')),
			UsernameField::create('Username', _t('TradeCoinAccountHistoryReport.USERNAME', 'Username')),
			TextField::create('Reference', _t('TradeCoinAccountHistoryReport.REFERENCE', 'Reference')),
			HiddenField::create('Submit', 'Submit', 1)
		);
	}
}
