<?php

/**
 * @package trading
 */

class TradeCoinAccountBalanceReport extends CustomReport {
	protected $sort = 60;
	
	public function title() {
		return _t('TradeCoinAccountBalanceReport.TITLE', 'Trade-Coin Account Balance Report');
	}
	
	public function getCMSFields() {
		$fields = parent::getCMSFields();

		$fields->dataFieldByName('Report')->setModelClass('TradeCoinAccount');
		
		return $fields;
	}
	
	public function sourceRecords($params, $sort, $limit) {
		if($sort) {
			$parts = explode(' ', $sort);
			$field = $parts[0];
			$direction = $parts[1];
			
			if($field == 'Name') {
			    $sort = 'FirstName ' . $direction;
			}
		}
        
		$where = array('Member.IsDistributor = 1');
        if(isset($params['StartBalance']) && is_numeric($params['StartBalance'])){
        	$where[] = sprintf("Balance >= %s", (int)$params['StartBalance']);
        }
		
		if(isset($params['EndBalance']) && is_numeric($params['EndBalance'])){
        	$where[] = sprintf("Balance <= %s", (int)$params['EndBalance']);
        }
        
        if(isset($params['Username']) && $params['Username']){
        	$where[] = sprintf("Username LIKE '%%%s%%'", $params['Username']);
        }
		
		$query = new SQLQuery();
		$query = $query->setFrom('TradeCoinAccountBalance')->setSelect(array('MemberID'))->setWhere($where)->setOrderBy($sort)->setLimit($limit)->addInnerJoin('Member', 'TradeCoinAccountBalance.MemberID = Member.ID')->selectField('TradeCoinAccountBalance.TotalDebit', 'Debit')->selectField('TradeCoinAccountBalance.TotalCredit', 'Credit')->selectField('TradeCoinAccountBalance.Balance', 'ForwardBalance');
		
		$returnSet = new ArrayList();
		foreach($query->execute() as $item){
			$data = TradeCoinAccount::create()->update($item);
			$returnSet->push($data);
		}

		return $returnSet;
	}

	public function columns() {
		$fields = array(
			'Member.Username' => array(
				'title' => _t('TradeCoinAccountBalanceReport.USERNAME', 'Username')
			),
			'Member.Name' => array(
				'title' => _t('TradeCoinAccountBalanceReport.NAME', 'Name')
			),
			'Credit' => array(
                'title' => _t('TradeCoinAccountBalanceReport.TOTAL_IN', 'Total In'),
                'casting' => 'Decimal->Nice'
            ),
            'Debit' => array(
                'title' => _t('TradeCoinAccountBalanceReport.TOTAL_OUT', 'Total Out'),
                'casting' => 'Decimal->Nice'
            ),
            'ForwardBalance' => array(
                'title' => _t('TradeCoinAccountBalanceReport.TOTAL_BALANCE', 'Total Balance'),
                'casting' => 'Decimal->Nice'
            )
		);
		
		return $fields;
	}
	
	public function parameterFields() {
		return new FieldList(
            FieldGroup::create(
            	NumericField::create('filters[StartBalance]', '')->addExtraClass('no-change-track'),
				NumericField::create('filters[EndBalance]', '')->addExtraClass('no-change-track')
			)->setTitle(_t('TradeCoinAccountBalanceReport.TOTAL_BALANCE', 'Total Balance')),
			UsernameField::create('Username', _t('TradeCoinAccountBalanceReport.USERNAME', 'Username'))
		);
	}
}
