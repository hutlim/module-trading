<?php
class CashConvertTradeCoinPage extends MemberPage {
	private static $singular_name = "E-Cash Convert Trade-Coin Page";
    private static $plural_name = "E-Cash Convert Trade-Coin Pages";
    private static $default_parent = 'AccountConvertPage';

    private static $db = array();

    private static $has_one = array();

}

class CashConvertTradeCoinPage_Controller extends MemberPage_Controller {

    /**
     * An array of actions that can be accessed via a request. Each array element
     * should be an action name, and the
     * permissions or conditions required to allow the user to access it.
     *
     * <code>
     * array (
     *     'action', // anyone can access this action
     *     'action' => true, // same as above
     *     'action' => 'ADMIN', // you must have ADMIN permissions to access this
     * action
     *     'action' => '->checkAction' // you can only access this action if
     * $this->checkAction() returns true
     * );
     * </code>
     *
     * @var array
     */
    private static $allowed_actions = array(
        'Form'
    );
	
	function init(){
		parent::init();
		$confirm_msg = _t('CashConvertTradeCoinPage.CONFIRM_MSG', 'Are you sure you want to proceed convert?');
		$js = <<<JS
			(function($) {
				$('form').submit(function(){
  					if (confirm("$confirm_msg")){
         				return true;
      				}
      				return false;
				});
			})(jQuery);
JS;
		Requirements::customScript($js, 'ConfirmConvert');
	}

    function Form() {
    	$fields = singleton('CashConvertTradeCoin')->getConvertFormFields($this->CurrentMember()->ID);
		
		$actions = FieldList::create(
            FormAction::create("doProceed", _t('CashConvertTradeCoinPage.BUTTONPROCEEDCONVERT', 'Proceed Convert'))
        );
		
		$validator = RequiredFields::create('Amount');
		
        return Form::create($this, 'Form', $fields, $actions, $validator);
    }
	
	function doProceed($data, $form) {
        try {
        	DB::getConn()->transactionStart();
        	$convert_id = CashConvertTradeCoin::create_statement($data, $this->CurrentMember()->ID);
            DB::getConn()->transactionEnd();
            return $this->redirect($this->CashConvertTradeCoinStatementLink);
        }
        catch(ValidationException $e) {
            DB::getConn()->transactionRollback();
            SS_Log::log(new Exception(print_r($e->getMessage(), true)), SS_Log::NOTICE);
            $form->sessionMessage($e->getMessage(), 'bad');
        }
        return $this->redirectBack();
    }
}