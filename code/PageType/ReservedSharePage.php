<?php
class ReservedSharePage extends MemberPage {
	private static $singular_name = "Reserved Share Page";
    private static $plural_name = "Reserved Share Pages";
	
    private static $default_parent = 'TradingPage';

    private static $db = array();

    private static $has_one = array(
		'TradeSetting' => 'TradeSetting'
	);

	function getSettingsFields() {
        $fields = parent::getSettingsFields();
        $fields->addFieldToTab('Root.Settings', DropdownField::create('TradeSettingID', _t('ReservedSharePage.TRADE_SETTING', 'Trade Setting'))->setSource(TradeSetting::get()->filter('IsActive', 1)->map()));
        return $fields;
    }
}

class ReservedSharePage_Controller extends MemberPage_Controller {

    /**
     * An array of actions that can be accessed via a request. Each array element
     * should be an action name, and the
     * permissions or conditions required to allow the user to access it.
     *
     * <code>
     * array (
     *     'action', // anyone can access this action
     *     'action' => true, // same as above
     *     'action' => 'ADMIN', // you must have ADMIN permissions to access this
     * action
     *     'action' => '->checkAction' // you can only access this action if
     * $this->checkAction() returns true
     * );
     * </code>
     *
     * @var array
     */
    private static $allowed_actions = array(
    	'Form', 
    	'request',
    	'RequestForm',
    	'view',
    	'ViewForm'
	);
	
	function Form() {
        $fields = FieldList::create();
		$actions = FieldList::create();
        $field_list = array(
            'Created' => _t('ReservedSharePage.DATE', 'Date'),
            'PurchaseShare.Price' => array('title' => _t('ReservedSharePage.PRICE', 'Price ({currency})', '', array('currency' => SiteCurrencyConfig::current_site_currency())), 'classes' => 'text-right'),
            'PurchaseShare.Unit' => array('title' => _t('ReservedSharePage.UNIT', 'Unit'), 'classes' => 'text-right'),
            'Amount' => array('title' => _t('ReservedSharePage.AMOUNT', 'Amount ({currency})', '', array('currency' => SiteCurrencyConfig::current_site_currency())), 'classes' => 'text-right'),
            'Remaining' => array('title' => _t('ReservedSharePage.REMAINING', 'Remaining ({currency})', '', array('currency' => SiteCurrencyConfig::current_site_currency())), 'classes' => 'text-right'),
            'Action' => array('title' => '', 'classes' => 'text-center')
        );
        $casting_list = array(
            'Created' => 'Datetime->Nice',
            'PurchaseShare.Unit' => 'Int->Formatted',
            'PurchaseShare.Price' => 'TradeCurrency->Nice',
            'Amount' => 'TradeCurrency->Nice',
            'Remaining' => 'TradeCurrency->Nice'
        );
        
        return DataListSearchForm::create($this, 'Form', 'ReservedShare', array('MemberID' => $this->CurrentMember()->ID, 'Remaining:GreaterThan' => 0, 'TradeSettingID' => $this->TradeSettingID), $fields, $actions)->setDataFieldList($field_list)->setFieldCasting($casting_list);
    }

	function request(){
		return $this->RequestForm()->forTemplate();
	}

    function RequestForm(){
    	$trade_setting = ($this->TradeSetting()->IsActive && $this->TradeSetting()->IsStarted && !$this->TradeSetting()->IsClosed) ? $this->TradeSetting() : false;
		if(!$trade_setting){
			$fields = FieldList::create(
				LiteralField::create('Note', _t('ReservedSharePage.REQUEST_TRADE_CLOSED', 'Sorry, currently trade market is closed for request trade.'))
			);
			
			$actions = FieldList::create();
			
			return Form::create($this, 'RequestForm', $fields, $actions);
		}
		
		if($trade_setting->RestrictMembers()->filter('TradeSetting_RestrictMembers.TradeRequest', 1)->byID($this->CurrentMember()->ID)){
			$trade_setting = false;
		}
		else {
			foreach($trade_setting->RestrictMembers()->filter('TradeSetting_RestrictMembers.TradeRequest', 1)->filter('TradeSetting_RestrictMembers.IncludeTeam', 1) as $member){
				$obj = Sponsor::get()->find('MemberID', $member->ID);
				if($obj && Sponsor::get()->filter('NLeft:GreaterThanOrEqual', (int)$obj->NLeft)->filter('NRight:LessThanOrEqual', (int)$obj->NRight)->filter('MemberID', $this->CurrentMember()->ID)->count()){
					$trade_setting = false;
					break;
				}
			}
		}
		
		if(!$trade_setting){
			$fields = FieldList::create(
				LiteralField::create('Note', _t('ReservedSharePage.RESTRICT_REQUEST_TRADE', 'Sorry, currently you\'re restrict for request trade.'))
			);
			
			$actions = FieldList::create();
			
			return Form::create($this, 'RequestForm', $fields, $actions);
		}
		
		$reserved_share = ReservedShare::get()->filter('MemberID', $this->CurrentMember()->ID)->byID($this->request->param('ID'));
        $fields = FieldList::create(
        	HiddenField::create('ReservedShareID', '', $reserved_share ? $reserved_share->ID : 0),
        	HtmlEditorField_Readonly::create('Remaining', _t('ReservedSharePage.REMAINING', 'Remaining'), $reserved_share ? $reserved_share->dbObject('Remaining')->Nice() : ''),
        	UsernameField::create('Username', _t('ReservedSharePage.USERNAME', 'Username')),
        	SellUnitField::create('SellUnit', 0, $trade_setting->Price, $trade_setting->SellFeePercentage)->setForcePrice(true)
        );
			
        $actions = FieldList::create(
            FormAction::create('doRequest', _t('ReservedSharePage.SEND_REQUEST', 'SEND REQUEST'))
        );
        
        $validators = RequiredFields::create('SellUnit');
        
        return Form::create($this, 'RequestForm', $fields, $actions, $validators);
    }

	function doRequest($data, $form){
        try {
        	DB::getConn()->transactionStart();
			$trade_setting = $this->TradeSetting();
			$trade_request = TradeRequest::create();
			$form->saveInto($trade_request);
			$trade_request->RequesterFee = $trade_request->Amount * $trade_setting->SellFeePercentage;
			$trade_request->ReceiverFee = $trade_request->Amount * $trade_setting->BuyFeePercentage;
			$trade_request->RequesterID = $this->CurrentMember()->ID;
			$trade_request->ReceiverID = Distributor::get_id_by_username($trade_request->Username);
			$trade_request->TradeSettingID = $trade_setting->ID;
			$trade_request->write();
			DB::getConn()->transactionEnd();
			$form->sessionMessage(_t('ReservedSharePage.SUCCESS_REQUEST', 'Sell request has been perform successfully'), 'success');
        }
        catch(ValidationException $e) {
        	DB::getConn()->transactionRollback();
            SS_Log::log(new Exception(print_r($e->getMessage(), true)), SS_Log::NOTICE);
            $form->sessionMessage($e->getResult()->message(), 'error');
        }
        return $this->redirect(Controller::join_links($this->Link('request'), $data['ReservedShareID']));
    }

	function view(){
		return $this->ViewForm()->forTemplate();
	}

	function ViewForm() {
        $fields = FieldList::create();
		$actions = FieldList::create();
        $field_list = array(
            'Created' => _t('ReservedSharePage.DATE', 'Date'),
            'Receiver.Username' => _t('ReservedSharePage.SEND_TO', 'Send To'),
            'Reference' => _t('ReservedSharePage.REFERENCE', 'Reference'),
            'Price' => array('title' => _t('ReservedSharePage.PRICE', 'Price ({currency})', '', array('currency' => SiteCurrencyConfig::current_site_currency())), 'classes' => 'text-right'),
            'Unit' => array('title' => _t('ReservedSharePage.UNIT', 'Unit'), 'classes' => 'text-right'),
            'Amount' => array('title' => _t('ReservedSharePage.AMOUNT', 'Amount ({currency})', '', array('currency' => SiteCurrencyConfig::current_site_currency())), 'classes' => 'text-right'),
            'RequesterFee' => array('title' => _t('ReservedSharePage.FEE', 'Fee ({currency})', '', array('currency' => SiteCurrencyConfig::current_site_currency())), 'classes' => 'text-right'),
            'CancelAction' => array('title' => '', 'classes' => 'text-center')
        );
        $casting_list = array(
            'Created' => 'Datetime->Nice',
            'Unit' => 'Int->Formatted',
            'Price' => 'TradeCurrency->Nice',
            'Amount' => 'TradeCurrency->Nice',
            'RequesterFee' => 'TradeCurrency->Nice'
        );
        
        return DataListSearchForm::create($this, 'ViewForm', 'TradeRequest', array('RequesterID' => $this->CurrentMember()->ID, 'ReservedShareID' => $this->request->requestVar('ID')), $fields, $actions)->setDataFieldList($field_list)->setFieldCasting($casting_list)->setFormAction(sprintf('%s?%s=%s', $this->Link('ViewForm'), 'ID', $this->request->requestVar('ID')));
    }
}
