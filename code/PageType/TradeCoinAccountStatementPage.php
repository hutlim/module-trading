<?php
class TradeCoinAccountStatementPage extends MemberPage {
	private static $singular_name = "Trade-Coin Account Statement Page";
    private static $plural_name = "Trade-Coin Account Statement Pages";
	
    private static $default_parent = 'TradingPage';

    private static $db = array();

    private static $has_one = array();

}

class TradeCoinAccountStatementPage_Controller extends MemberPage_Controller {

    /**
     * An array of actions that can be accessed via a request. Each array element
     * should be an action name, and the
     * permissions or conditions required to allow the user to access it.
     *
     * <code>
     * array (
     *     'action', // anyone can access this action
     *     'action' => true, // same as above
     *     'action' => 'ADMIN', // you must have ADMIN permissions to access this
     * action
     *     'action' => '->checkAction' // you can only access this action if
     * $this->checkAction() returns true
     * );
     * </code>
     *
     * @var array
     */
    private static $allowed_actions = array('Form');

    public function init() {
        parent::init();
    }

    function Form() {
        $fields = FieldList::create(
            HtmlEditorField_Readonly::create('TradeCoinAccountBalance', '', sprintf('<b>%s</b>', _t('TradeCoinAccountStatementPage.CURRENT_BALANCE', 'Current Balance: {balance}', 'Display Trade-Coin account balance', array('balance' => Account::get('TradeCoinAccount', $this->CurrentMember()->ID)->obj('Balance')->Nice())))),
            DateRangeField::create('Date', _t('TradeCoinAccountStatementPage.DATE', 'Date')),
            singleton('TradeCoinAccountType')->getDropdownField('Type', _t('TradeCoinAccountStatementPage.TYPE', 'Type'))->setEmptyString(_t('TradeCoinAccountStatementPage.ALL', 'All'))
        );

        $field_list = array(
            'Date' => _t('TradeCoinAccountStatementPage.DATE', 'Date'),
            'TypeByLang' => _t('TradeCoinAccountStatementPage.TYPE', 'Type'),
            'Description' => _t('TradeCoinAccountStatementPage.TRANSACTION_DESCRIPTION', 'Description'),
            'Reference' => _t('TradeCoinAccountStatementPage.REFERENCE', 'Reference'),
            'Credit' => array('title' => _t('TradeCoinAccountStatementPage.IN', 'In'), 'classes' => 'text-right'),
            'Debit' => array('title' => _t('TradeCoinAccountStatementPage.OUT', 'Out'), 'classes' => 'text-right'),
            'ForwardBalance' => array('title' => _t('TradeCoinAccountStatementPage.BALANCE', 'Balance'), 'classes' => 'text-right')
        );
        $casting_list = array(
            'Date' => 'SS_Datetime->Nice',
            'Credit' => 'TradeCurrency->Nice',
            'Debit' => 'TradeCurrency->Nice',
            'ForwardBalance' => 'TradeCurrency->Nice'
        );
        
        return DataListSearchForm::create($this, 'Form', 'TradeCoinAccount', array('MemberID' => $this->CurrentMember()->ID), $fields)->setDataFieldList($field_list)->setFieldCasting($casting_list);
    }
}
