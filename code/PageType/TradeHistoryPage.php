<?php
class TradeHistoryPage extends MemberPage {
	private static $singular_name = "Trade History Page";
    private static $plural_name = "Trade Hstory Pages";
	
    private static $default_parent = 'TradingPage';

    private static $db = array();

    private static $has_one = array(
		'TradeSetting' => 'TradeSetting'
	);

	function getSettingsFields() {
        $fields = parent::getSettingsFields();
        $fields->addFieldToTab('Root.Settings', DropdownField::create('TradeSettingID', _t('TradeHistoryPage.TRADE_SETTING', 'Trade Setting'))->setSource(TradeSetting::get()->filter('IsActive', 1)->map()));
        return $fields;
    }
}

class TradeHistoryPage_Controller extends MemberPage_Controller {

    /**
     * An array of actions that can be accessed via a request. Each array element
     * should be an action name, and the
     * permissions or conditions required to allow the user to access it.
     *
     * <code>
     * array (
     *     'action', // anyone can access this action
     *     'action' => true, // same as above
     *     'action' => 'ADMIN', // you must have ADMIN permissions to access this
     * action
     *     'action' => '->checkAction' // you can only access this action if
     * $this->checkAction() returns true
     * );
     * </code>
     *
     * @var array
     */
    private static $allowed_actions = array('Form');

    public function init() {
        parent::init();
    }

    function Form() {
        $fields = FieldList::create(
            DateRangeField::create('Created', _t('TradeHistoryPage.DATE', 'Date')),
            DropdownField::create('Type', _t('TradeHistoryPage.TYPE', 'Type'), singleton('TradeHistory')->dbObject('Type')->enumValues())->setEmptyString(_t('TradeHistoryPage.ALL', 'All'))
        );

        $field_list = array(
            'Created' => _t('TradeHistoryPage.DATE', 'Date'),
            'Type' => _t('TradeHistoryPage.TYPE', 'Type'),
            'Reference' => _t('TradeHistoryPage.REFERENCE', 'Reference'),
            'Price' => array('title' => _t('TradeHistoryPage.UNIT_PRICE', 'Unit Price ({currency})', '', array('currency' => SiteCurrencyConfig::current_site_currency())), 'classes' => 'text-right'),
            'Unit' => array('title' => _t('TradeHistoryPage.UNIT', 'Unit'), 'classes' => 'text-right'),
            'Amount' => array('title' => _t('TradeHistoryPage.AMOUNT', 'Amount ({currency})', '', array('currency' => SiteCurrencyConfig::current_site_currency())), 'classes' => 'text-right'),
            'Fee' => array('title' => _t('TradeHistoryPage.FEE', 'Fee ({currency})', '', array('currency' => SiteCurrencyConfig::current_site_currency())), 'classes' => 'text-right')
        );
        $casting_list = array(
            'Created' => 'SS_Datetime->Nice',
            'Price' => 'TradeCurrency->Nice',
            'Unit' => 'Int->Formatted',
            'Amount' => 'TradeCurrency->Nice',
            'Fee' => 'TradeCurrency->Nice'
        );
        
        return DataListSearchForm::create($this, 'Form', 'TradeHistory', array('MemberID' => $this->CurrentMember()->ID, 'TradeSettingID' => $this->TradeSettingID), $fields)->setDataFieldList($field_list)->setFieldCasting($casting_list);
    }
}
