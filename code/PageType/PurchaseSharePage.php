<?php
class PurchaseSharePage extends MemberPage {
	private static $singular_name = "Purchase Share Page";
    private static $plural_name = "Purchase Share Pages";
	
    private static $default_parent = 'TradingPage';

    private static $db = array();

    private static $has_one = array(
		'TradeSetting' => 'TradeSetting'
	);

	function getSettingsFields() {
        $fields = parent::getSettingsFields();
        $fields->addFieldToTab('Root.Settings', DropdownField::create('TradeSettingID', _t('PurchaseSharePage.TRADE_SETTING', 'Trade Setting'))->setSource(TradeSetting::get()->filter('IsActive', 1)->map()));
        return $fields;
    }
}

class PurchaseSharePage_Controller extends MemberPage_Controller {

    /**
     * An array of actions that can be accessed via a request. Each array element
     * should be an action name, and the
     * permissions or conditions required to allow the user to access it.
     *
     * <code>
     * array (
     *     'action', // anyone can access this action
     *     'action' => true, // same as above
     *     'action' => 'ADMIN', // you must have ADMIN permissions to access this
     * action
     *     'action' => '->checkAction' // you can only access this action if
     * $this->checkAction() returns true
     * );
     * </code>
     *
     * @var array
     */
    private static $allowed_actions = array(
    	'PurchaseShareForm',
    	'MarketActivityForm',
    	'chart_json'
	);
	
	function index(){
		Requirements::combine_files('highstock.js', array(
			'trading/thirdparty/highstock/highstock.js',
			'trading/thirdparty/highstock/highcharts-more.js',
			'trading/thirdparty/highstock/exporting.js'
		));
		Requirements::javascript('trading/javascript/PurchaseSharePage.js');
		Requirements::css('trading/css/PurchaseSharePage.css');
		return $this->render();
	}

    function PurchaseShareForm(){
    	$trade_setting = ($this->TradeSetting()->IsActive && $this->TradeSetting()->IsStarted && !$this->TradeSetting()->IsClosed) ? $this->TradeSetting() : false;
		if(!$trade_setting){
			$fields = FieldList::create(
				LiteralField::create('Note', _t('PurchaseSharePage.PURCHASE_SHARE_CLOSED', 'Sorry, currently trade market is closed for purchase.'))
			);
			
			$actions = FieldList::create();
			
			return Form::create($this, 'PurchaseShareForm', $fields, $actions);
		}
		
		if($trade_setting->RestrictMembers()->filter('TradeSetting_RestrictMembers.PurchaseShare', 1)->byID($this->CurrentMember()->ID)){
			$trade_setting = false;
		}
		else {
			foreach($trade_setting->RestrictMembers()->filter('TradeSetting_RestrictMembers.PurchaseShare', 1)->filter('TradeSetting_RestrictMembers.IncludeTeam', 1) as $member){
				$obj = Sponsor::get()->find('MemberID', $member->ID);
				if($obj && Sponsor::get()->filter('NLeft:GreaterThanOrEqual', (int)$obj->NLeft)->filter('NRight:LessThanOrEqual', (int)$obj->NRight)->filter('MemberID', $this->CurrentMember()->ID)->count()){
					$trade_setting = false;
					break;
				}
			}
		}
		
		if(!$trade_setting){
			$fields = FieldList::create(
				LiteralField::create('Note', _t('PurchaseSharePage.RESTRICT_PURCHASE_SHARE', 'Sorry, currently you\'re restrict for purchase {title}.', '', array('title' => $this->TradeSetting()->Title)))
			);
			
			$actions = FieldList::create();
			
			return Form::create($this, 'PurchaseShareForm', $fields, $actions);
		}
		
		$available_buy_unit = $this->CurrentMember()->TradePointAccountBalance ? $this->CurrentMember()->TradePointAccountBalance / $trade_setting->Price : 0;
        $fields = FieldList::create(
        	HtmlEditorField_Readonly::create('AvailableTradePoint', _t('PurchaseSharePage.TRADE_POINT_BALANCE', 'Trade-Point Balance'), $this->CurrentMember()->obj('TradePointAccountBalance')->Nice()),
        	HtmlEditorField_Readonly::create('CurrentPrice', _t('PurchaseSharePage.CURRENT_PRICE', 'Current Price'), $trade_setting->dbObject('Price')->Nice()),
        	HtmlEditorField_Readonly::create('CurrentValue', _t('PurchaseSharePage.CURRENT_VALUE', 'Current Value'), sprintf('%s (%s %s)', $this->CurrentMember()->TradeAccount($trade_setting->ID)->obj('CurrentValue')->Nice(), $this->CurrentMember()->TradeAccount($trade_setting->ID)->obj('TradeDeposit')->Nice(), _t('PurchaseSharePage.RESERVED', 'Reserved'))),
        	BuyUnitField::create('PurchaseUnit', $available_buy_unit, $trade_setting->Price, $trade_setting->BuyFeePercentage)->setForcePrice(true)
        );
			
        $actions = FieldList::create(
            FormAction::create('doPurchase', _t('PurchaseSharePage.PURCHASE', 'PURCHASE'))
        );
        
        $validators = RequiredFields::create('PurchaseUnit');
        
        return Form::create($this, 'PurchaseShareForm', $fields, $actions, $validators);
    }

	function doPurchase($data, $form){
        try {
        	DB::getConn()->transactionStart();
			$trade = PurchaseShare::create();
			$form->saveInto($trade);
			$trade->MemberID = $this->CurrentMember()->ID;
			$trade->TradeSettingID = $this->TradeSettingID;
			$trade->write();
			DB::getConn()->transactionEnd();
			$form->sessionMessage(_t('PurchaseSharePage.SUCCESS_PURCHASE', '{title} has been purchase successfully', '', array('title' => $this->TradeSetting()->Title)), 'success');
        }
        catch(ValidationException $e) {
        	DB::getConn()->transactionRollback();
            SS_Log::log(new Exception(print_r($e->getMessage(), true)), SS_Log::NOTICE);
            $form->sessionMessage($e->getResult()->message(), 'error');
        }
        return $this->redirectBack();
    }

	function MarketActivityForm() {
        $fields = FieldList::create();
		$actions = FieldList::create();
        $field_list = array(
            'Created' => _t('PurchaseSharePage.DATE', 'Date'),
            'Price' => array('title' => _t('PurchaseSharePage.UNIT_PRICE', 'Unit Price ({currency})', '', array('currency' => SiteCurrencyConfig::current_site_currency())), 'classes' => 'text-right text-green'),
            'Unit' => array('title' => _t('PurchaseSharePage.UNIT', 'Unit'), 'classes' => 'text-right'),
            'Amount' => array('title' => _t('PurchaseSharePage.AMOUNT', 'Amount ({currency})', '', array('currency' => SiteCurrencyConfig::current_site_currency())), 'classes' => 'text-right text-green')
        );
        $casting_list = array(
            'Created' => 'Datetime->Nice',
            'Price' => 'TradeCurrency->Nice',
            'Unit' => 'Int->Formatted',
            'Amount' => 'TradeCurrency->Nice'
        );
        
        return DataListSearchForm::create($this, 'MarketActivityForm', 'TradeHistory', array('Type' => 'Buy', 'TradeSettingID' => $this->TradeSettingID), $fields, $actions)->setDataFieldList($field_list)->setFieldCasting($casting_list)->setSourceLimit(20);
    }

	function chart_json(){
		$items = GroupedList::create(TradeHistory::get()->filter('Type', 'Buy')->filter('Created:GreaterThanOrEqual', date("Y-m-d 00:00:00", strtotime("-1 months")))->filter('TradeSettingID', $this->TradeSettingID)->sort('Created'));
		$price_data = array();
		$volume_data = array();
		foreach($items->groupBy('Created') as $group_items){
			$volume = 0;
			foreach($group_items as $item){
				$volume += $item->Unit;
			}
			$price_data[] = array(
				strtotime($group_items->first()->Created) * 1000,
				floatval($group_items->first()->Price)
			);
			
			$volume_data[] = array(
				strtotime($group_items->first()->Created) * 1000,
				$volume
			);
		}

		$price_data = array('name' => _t('PurchaseSharePage.PRICE', 'Price'), 'type' => 'spline', 'yAxis' => 0, 'data' => $price_data);
		$volume_data = array('name' => _t('PurchaseSharePage.TRADE_VOLUME', 'Trade Volume'), 'type' => 'column', 'yAxis' => 1, 'data' => $volume_data);
		return Convert::array2json(array($price_data, $volume_data));
	}
}
