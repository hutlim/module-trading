<?php
class TradePointAccountStatementPage extends MemberPage {
	private static $singular_name = "Trade-Point Account Statement Page";
    private static $plural_name = "Trade-Point Account Statement Pages";
	
    private static $default_parent = 'TradingPage';

    private static $db = array();

    private static $has_one = array();

}

class TradePointAccountStatementPage_Controller extends MemberPage_Controller {

    /**
     * An array of actions that can be accessed via a request. Each array element
     * should be an action name, and the
     * permissions or conditions required to allow the user to access it.
     *
     * <code>
     * array (
     *     'action', // anyone can access this action
     *     'action' => true, // same as above
     *     'action' => 'ADMIN', // you must have ADMIN permissions to access this
     * action
     *     'action' => '->checkAction' // you can only access this action if
     * $this->checkAction() returns true
     * );
     * </code>
     *
     * @var array
     */
    private static $allowed_actions = array('Form');

    public function init() {
        parent::init();
    }

    function Form() {
        $fields = FieldList::create(
            HtmlEditorField_Readonly::create('TradePointAccountBalance', '', sprintf('<b>%s</b>', _t('TradePointAccountStatementPage.CURRENT_BALANCE', 'Current Balance: {balance}', 'Display Trade-Point account balance', array('balance' => Account::get('TradePointAccount', $this->CurrentMember()->ID)->obj('Balance')->Nice())))),
            DateRangeField::create('Date', _t('TradePointAccountStatementPage.DATE', 'Date')),
            singleton('TradePointAccountType')->getDropdownField('Type', _t('TradePointAccountStatementPage.TYPE', 'Type'))->setEmptyString(_t('TradePointAccountStatementPage.ALL', 'All'))
        );

        $field_list = array(
            'Date' => _t('TradePointAccountStatementPage.DATE', 'Date'),
            'TypeByLang' => _t('TradePointAccountStatementPage.TYPE', 'Type'),
            'Description' => _t('TradePointAccountStatementPage.TRANSACTION_DESCRIPTION', 'Description'),
            'Reference' => _t('TradePointAccountStatementPage.REFERENCE', 'Reference'),
            'Credit' => array('title' => _t('TradePointAccountStatementPage.IN', 'In'), 'classes' => 'text-right'),
            'Debit' => array('title' => _t('TradePointAccountStatementPage.OUT', 'Out'), 'classes' => 'text-right'),
            'ForwardBalance' => array('title' => _t('TradePointAccountStatementPage.BALANCE', 'Balance'), 'classes' => 'text-right')
        );
        $casting_list = array(
            'Date' => 'SS_Datetime->Nice',
            'Credit' => 'TradeCurrency->Nice',
            'Debit' => 'TradeCurrency->Nice',
            'ForwardBalance' => 'TradeCurrency->Nice'
        );
        
        return DataListSearchForm::create($this, 'Form', 'TradePointAccount', array('MemberID' => $this->CurrentMember()->ID), $fields)->setDataFieldList($field_list)->setFieldCasting($casting_list);
    }
}
