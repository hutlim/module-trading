<?php
class TradeRequestPage extends MemberPage {
	private static $singular_name = "Trade Request Page";
    private static $plural_name = "Trade Request Pages";
	
    private static $default_parent = 'TradingPage';

    private static $db = array();

    private static $has_one = array(
		'TradeSetting' => 'TradeSetting'
	);

	function getSettingsFields() {
        $fields = parent::getSettingsFields();
        $fields->addFieldToTab('Root.Settings', DropdownField::create('TradeSettingID', _t('TradeRequestPage.TRADE_SETTING', 'Trade Setting'))->setSource(TradeSetting::get()->filter('IsActive', 1)->map()));
        return $fields;
    }
}

class TradeRequestPage_Controller extends MemberPage_Controller {

    /**
     * An array of actions that can be accessed via a request. Each array element
     * should be an action name, and the
     * permissions or conditions required to allow the user to access it.
     *
     * <code>
     * array (
     *     'action', // anyone can access this action
     *     'action' => true, // same as above
     *     'action' => 'ADMIN', // you must have ADMIN permissions to access this
     * action
     *     'action' => '->checkAction' // you can only access this action if
     * $this->checkAction() returns true
     * );
     * </code>
     *
     * @var array
     */
    private static $allowed_actions = array(
    	'SellRequestForm', 
    	'cancel',
    	'BuyRequestForm', 
    	'accept'
	);
	
	function index(){
		Requirements::css('trading/css/TradeRequestPage.css');
		return $this->render();
	}
	
	function SellRequestForm() {
        $fields = FieldList::create();
		$actions = FieldList::create();
        $field_list = array(
            'Created' => _t('TradeRequestPage.DATE', 'Date'),
            'Receiver.Username' => _t('TradeRequestPage.SEND_TO', 'Send To'),
            'Reference' => _t('TradeRequestPage.REFERENCE', 'Reference'),
            'Price' => array('title' => _t('TradeRequestPage.PRICE', 'Price ({currency})', '', array('currency' => SiteCurrencyConfig::current_site_currency())), 'classes' => 'text-right'),
            'Unit' => array('title' => _t('TradeRequestPage.UNIT', 'Unit'), 'classes' => 'text-right'),
            'Amount' => array('title' => _t('TradeRequestPage.AMOUNT', 'Amount ({currency})', '', array('currency' => SiteCurrencyConfig::current_site_currency())), 'classes' => 'text-right'),
            'RequesterFee' => array('title' => _t('TradeRequestPage.FEE', 'Fee ({currency})', '', array('currency' => SiteCurrencyConfig::current_site_currency())), 'classes' => 'text-right'),
            'CancelAction' => array('title' => '', 'classes' => 'text-center')
        );
        $casting_list = array(
            'Created' => 'Datetime->Nice',
            'Unit' => 'Int->Formatted',
            'Price' => 'TradeCurrency->Nice',
            'Amount' => 'TradeCurrency->Nice',
            'RequesterFee' => 'TradeCurrency->Nice'
        );
        
        return DataListSearchForm::create($this, 'SellRequestForm', 'TradeRequest', array('RequesterID' => $this->CurrentMember()->ID, 'TradeSettingID' => $this->TradeSettingID), $fields, $actions)->setDataFieldList($field_list)->setFieldCasting($casting_list);
    }

	function cancel(){
		if($trade_request = TradeRequest::get()->filter('RequesterID', $this->CurrentMember()->ID)->byID($this->request->param('ID'))){
			try {
	        	DB::getConn()->transactionStart();
				$trade_request->Status = 'Cancelled';
				$trade_request->write();
				DB::getConn()->transactionEnd();
				$this->setMessage('success', _t('TradeRequestPage.SUCCESS_CANCEL', 'Sell request have been cancelled'));
	        }
	        catch(ValidationException $e) {
	        	DB::getConn()->transactionRollback();
	            SS_Log::log(new Exception(print_r($e->getMessage(), true)), SS_Log::NOTICE);
				$this->setMessage('error', $e->getResult()->message());
	        }
			return $this->redirectBack();
		}
		
		return $this->httpError('404');
	}
	
	function BuyRequestForm() {
        $fields = FieldList::create();
		$actions = FieldList::create();
        $field_list = array(
            'Created' => _t('TradeRequestPage.DATE', 'Date'),
            'Requester.Username' => _t('TradeRequestPage.SEND_FROM', 'Send From'),
            'Reference' => _t('TradeRequestPage.REFERENCE', 'Reference'),
            'Price' => array('title' => _t('TradeRequestPage.PRICE', 'Price ({currency})', '', array('currency' => SiteCurrencyConfig::current_site_currency())), 'classes' => 'text-right'),
            'Unit' => array('title' => _t('TradeRequestPage.UNIT', 'Unit'), 'classes' => 'text-right'),
            'Amount' => array('title' => _t('TradeRequestPage.AMOUNT', 'Amount ({currency})', '', array('currency' => SiteCurrencyConfig::current_site_currency())), 'classes' => 'text-right'),
            'ReceiverFee' => array('title' => _t('TradeRequestPage.FEE', 'Fee ({currency})', '', array('currency' => SiteCurrencyConfig::current_site_currency())), 'classes' => 'text-right'),
            'AcceptAction' => array('title' => '', 'classes' => 'text-center')
        );
        $casting_list = array(
            'Created' => 'Datetime->Nice',
            'Unit' => 'Int->Formatted',
            'Price' => 'TradeCurrency->Nice',
            'Amount' => 'TradeCurrency->Nice',
            'ReceiverFee' => 'TradeCurrency->Nice'
        );
        
        return DataListSearchForm::create($this, 'BuyRequestForm', 'TradeRequest', array('ReceiverID' => $this->CurrentMember()->ID, 'TradeSettingID' => $this->TradeSettingID), $fields, $actions)->setDataFieldList($field_list)->setFieldCasting($casting_list);
    }

	function accept(){
		if($trade_request = TradeRequest::get()->filter('ReceiverID', $this->CurrentMember()->ID)->byID($this->request->param('ID'))){
			try {
	        	DB::getConn()->transactionStart();
				$trade_request->Status = 'Accepted';
				$trade_request->write();
				DB::getConn()->transactionEnd();
				$this->setMessage('success', _t('TradeRequestPage.SUCCESS_ACCEPT', 'Buy request have been accepted'));
	        }
	        catch(ValidationException $e) {
	        	DB::getConn()->transactionRollback();
	            SS_Log::log(new Exception(print_r($e->getMessage(), true)), SS_Log::NOTICE);
				$this->setMessage('error', $e->getResult()->message());
	        }
			return $this->redirectBack();
		}
		
		return $this->httpError('404');
	}
}
