<% include SideBar %>
<div class="typography col-sm-8">
    <div class="well">
        $Message
        <article>
    		<h1>$Title</h1>
    		<div class="content">$Content</div>
    	</article>
		<div class="panel panel-primary">
  			<div class="panel-heading"><%t PurchaseSharePage.ss.CHART_TITLE "Price And Volume In This Year" %></div>
  			<div class="panel-body">
    			<div id="trade-chart" data-url="$Link(chart_json)" data-price-title="<%t PurchaseSharePage.ss.PRICE_TITLE "Price" %>" data-volume-title="<%t PurchaseSharePage.ss.VOLUME_TITLE "Volume" %>"><%t PurchaseSharePage.ss.LOADING_DATA "Loading data..." %></div>
  			</div>
		</div>
		<div class="panel panel-green">
  			<div class="panel-heading"><%t PurchaseSharePage.ss.PURCHASE_SHARE "Purchase {title}" title=$TradeSetting.Title %></div>
  			<div class="panel-body">
    			$PurchaseShareForm
  			</div>
		</div>
		<div class="panel panel-primary">
  			<div class="panel-heading"><%t PurchaseSharePage.ss.RECENT_MARKET_ACTIVITIES "Recent Market Activities" %></div>
  			<div class="panel-body">
    			$MarketActivityForm
  			</div>
		</div>
    </div>
</div>