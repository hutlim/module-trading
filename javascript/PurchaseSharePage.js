(function($) {
	var options = {
		chart: {
			renderTo: 'trade-chart',
	        borderWidth: 1
	    },
        exporting: {
            buttons: {
                contextButton: {
                    enabled: true
                }
            }
        },
        rangeSelector:{
        	buttons: [{
                type: 'minute',
                count: 60,
                text: '1H'
            }, {
                type: 'minute',
                count: 240,
                text: '4H'
            }, {
                type: 'minute',
                count: 360,
                text: '6H'
            }, {
                type: 'day',
                count: 1,
                text: '1D'
            }, {
                type: 'week',
                count: 1,
                text: '1W'
            }, {
                type: 'month',
                count: 1,
                text: '1M'
            }],
            selected: 4,
            inputEnabled: false
        },
        yAxis: [
        	{ // Primary yAxis
        		title: {
	                text: $('#trade-chart').data('price-title'),
            		useHTML: Highcharts.hasBidiBug
	            },
	            labels: {
	            	align: 'right',
                    x: -3,
	                format: '{value}'
	            },
	            height: '30%',
                lineWidth: 2
        	}, 
        	{ // Secondary yAxis
	            title: {
	                text: $('#trade-chart').data('volume-title'),
	                useHTML: Highcharts.hasBidiBug
	            },
	            labels: {
	            	align: 'right',
                    x: -3,
	                format: '{value}'
	            },
	            top: '35%',
                height: '65%',
                offset: 0,
                lineWidth: 2
            }
		],
        tooltip: {
            formatter: function() {
                var s = '<b>'+ Highcharts.dateFormat('%A, %b %e, %Y, %l:%M %p', this.x) +'</b>';

				$.each(this.points, function(i, point) {
                    if(i == 0) {
					    s += '<br/>Price = '+ point.y;
                    } else {
                        s += '<br/>Volume = '+ point.y;
                    }
				});
				return s;
			},
			useHTML: true
        },
        series: [{}]
    };
    
    var url = $('#trade-chart').data('url');
    $.getJSON(url,  function(data) {
        options.series = data;
        var chart = new Highcharts.StockChart(options);
    });
    
})(jQuery);