(function($) {
	$("div.field.buyunit").find('input.buy-unit').live("keypress keyup", function(e) {
		e.preventDefault();
		var field = $(this);
		var form_container = field.parents('form');
		var buy_container = field.parents('div.field.buyunit');
		var total_field = buy_container.find('input.buy-total');
		var fee_field = buy_container.find('input.buy-fee');
		var payable_field = buy_container.find('input.buy-payable');
		var price = buy_container.find('input.buy-price').val();
		var fee_percentage = buy_container.find('input.buy-fee-percentage').val();
		if(field.val() == ''){
			total_field.val('');
		    fee_field.val('');
		    payable_field.val('');
			return false;
		}
		
		var total = field.val() * price;
		var fee = total * fee_percentage;
		var payable = total + fee;
		total_field.val($.inputmask.format(total, {alias: 'decimal', digits: 3, digitsOptional: false, placeholder: '0', 'prefix': total_field.data('prefix')}));
	    fee_field.val($.inputmask.format(total * fee_percentage, {alias: 'decimal', digits: 3, digitsOptional: false, placeholder: '0', 'prefix': fee_field.data('prefix')}));
	    payable_field.val($.inputmask.format(payable, {alias: 'decimal', digits: 3, digitsOptional: false, placeholder: '0', 'prefix': payable_field.data('prefix')}));
	});
})(jQuery);
