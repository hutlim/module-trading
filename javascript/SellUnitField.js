(function($) {
	$("div.field.sellunit").find('input.sell-unit').live("keypress keyup", function(e) {
		e.preventDefault();
		var field = $(this);
		var form_container = field.parents('form');
		var sell_container = field.parents('div.field.sellunit');
		var total_field = sell_container.find('input.sell-total');
		var fee_field = sell_container.find('input.sell-fee');
		var receivable_field = sell_container.find('input.sell-receivable');
		var price = sell_container.find('input.sell-price').val();
		var fee_percentage = sell_container.find('input.sell-fee-percentage').val();
		if(field.val() == ''){
			total_field.val('');
		    fee_field.val('');
		    receivable_field.val('');
			return false;
		}
		
		var total = field.val() * price;
		var fee = total * fee_percentage;
		var receivable = total - fee;
		total_field.val($.inputmask.format(total, {alias: 'decimal', digits: 3, digitsOptional: false, placeholder: '0', 'prefix': total_field.data('prefix')}));
	    fee_field.val($.inputmask.format(total * fee_percentage, {alias: 'decimal', digits: 3, digitsOptional: false, placeholder: '0', 'prefix': fee_field.data('prefix')}));
	    receivable_field.val($.inputmask.format(receivable, {alias: 'decimal', digits: 3, digitsOptional: false, placeholder: '0', 'prefix': receivable_field.data('prefix')}));
	});
})(jQuery);
